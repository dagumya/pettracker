/**
 * Created by Brittany on 10/22/17, updated to better match testPet_Branch and testUser codes
 */
process.env.MONGO_LOCAL = "localhost/animal_tracker_test";
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');
var should = chai.should();
chai.use(chaiHttp);

/**
 * IMPORT MODELS TO SAVE TEST DATA BEFORE RUNNING TESTS
 */
var userService = require("../services/user.service.server");
var branchService = require("../services/pet.branch.service.server");


var sample_david = {
    username: "dagumya",
    password: "pas345yet",
    firstName: "david",
    lastName: "geoffey",
    email: "geod@gmail.com",
    phone: "8964563452",
    streetAddress: "123 Common Street",
    city: "Seattle",
    state: "Washington",
    zipcode: 34521
};
var sample_susan = {
    username: "susang",
    password: "pas3125yet",
    firstName: "Susan",
    lastName: "Mcdonald",
    email: "susam@gmail.com",
    phone: "6348964552",
    streetAddress: "123 Test Street",
    city: "New York",
    state: "New York",
    zipcode: 34521
};
var sample_branch1 = {
    name: "Pet Republic",
    branchUniqueId: "PR04",
    noOfStaff: 34,
    streetAddress: "875 West Beach Rd",
    city: "San Franscisco",
    state: "California",
    zipcode: 67534,
    contact: "8745623452",
    active: true,
    inventory: [
        {
            category: "SMALL",
            capacity: 20,
            allocated: 10,
            availability: 10
        },
        {
            category: "MEDIUM",
            capacity: 10,
            allocated: 2,
            availability: 8
        },
        {
            category: "LARGE",
            capacity: 30,
            allocated: 5,
            availability: 25
        }
    ]
};
var sample_branch2 = {
    name: "Pet Colony",
    branchUniqueId: "PC01",
    noOfStaff: 3,
    streetAddress: "875 Circuit Rd",
    city: "Atlanta",
    state: "Georgia",
    zipcode: 45357,
    contact: "8896782313",
    active: true,
    inventory: [
        {
            category: "SMALL",
            capacity: 15,
            allocated: 10,
            availability: 5
        },
        {
            category: "MEDIUM",
            capacity: 6,
            allocated: 2,
            availability: 4
        },
        {
            category: "LARGE",
            capacity: 20,
            allocated: 5,
            availability: 15
        }
    ]
};

var sample_branch3 = {
    name: "Pet Imporium",
    branchUniqueId: "PI01",
    noOfStaff: 3,
    streetAddress: "8 Crescent Rd",
    city: "Chicago",
    state: "Illinois",
    zipcode: 45767,
    contact: "8896782313",
    active: true,
    inventory: [
        {
            category: "SMALL",
            capacity: 15,
            allocated: 15,
            availability: 0
        },
        {
            category: "MEDIUM",
            capacity: 6,
            allocated: 2,
            availability: 4
        },
        {
            category: "LARGE",
            capacity: 20,
            allocated: 5,
            availability: 15
        }
    ]
};

var user_input1 = {
    microchipId: "A010",
    name: "Popoloski",
    species: "CAT",
    gender: "FEMALE",
    breed: "Funny Cats",
    category: "SMALL",
    weight: 10,
    age: 4,
    colorDescription: "Jet Black",
    personality: "Mysterious",
    notes: "Has scar across eye",
    intakeDate: "1/4/2017",
    neutered: false,
    disabled: false,
    imageUrl: "/images/test",
    latitude: 41.40338,
    longitude: 10.4418,
    foundBy: "dagumya",
    unique_branch_id: "PC01",
    status: "AVAILABLE"
};
var user_input2 = {
    microchipId: "A020",
    name: "Mischief",
    species: "DOG",
    gender: "MALE",
    breed: "Persian",
    category: "MEDIUM",
    weight: 15,
    age: 2,
    colorDescription: "Brown with white spots",
    personality: "Very Playful",
    notes: "Rescued",
    intakeDate: "5/7/2016",
    neutered: true,
    disabled: false,
    imageUrl: "/images/test1",
    latitude: 78.40338,
    longitude: 23.4418,
    foundBy: "susang",
    unique_branch_id: "PR04",
    status: "AVAILABLE"
};
var user_input3 = {
    microchipId: "A030",
    name: "Danny the Menace",
    species: "DOG",
    gender: "MALE",
    breed: "Pug",
    category: "SMALL", // branch at capacity
    weight: 10,
    age: 3,
    colorDescription: "White",
    personality: "Very Jovial and loving. Clingy ",
    notes: "Rescued",
    intakeDate: "5/9/2016",
    neutered: true,
    disabled: false,
    imageUrl: "/images/test4",
    latitude: 78.40338,
    longitude: 23.4418,
    foundBy: "susang",
    unique_branch_id: "PI01",
    status: "UNAVAILABLE"
};

var user_input5 = {
    microchipId: "A050",
    name: "Lucy",
    species: "CAT",
    gender: "MALE",
    breed: "Persian",
    category: "MEDIUM",
    weight: 16,
    age: 5,
    colorDescription: "Brown",
    personality: "Very Jovial and loving. Clingy ",
    notes: "Rescued",
    intakeDate: "5/9/2016",
    neutered: true,
    disabled: false,
    imageUrl: "/images/test3",
    latitude: 78.40338,
    longitude: 23.4418,
    foundBy: "mildred", // foundby  not in system
    unique_branch_id: "PI01",
    status: "AVAILABLE"
};
var pet_updates = {
    name: "Tommy",
    breed: "RagDoll",
    neutered: true
};

var sample_employee = {
    username: "employee2",
    password: "pas345yet",
    apiKey: "dsghe645bvds",
    firstName: "employee",
    lastName: "employee",
    email: "geod@gmail.com",
    phone: "8964563452",
    streetAddress: "123 Common Street",
    city: "Seattle",
    state: "Washington",
    zipcode: 34521,
    role: ['EMPLOYEE'],
    active: true

};

var employeeApiKey = "dsghe645bvds";
var user1ApiKey = "";
describe("Test Pet Routes", function () {

    before(function (done) {
        // use usermodel and pet model to save the sample users and branches
        userService.createUser(sample_david).then(function (user) {
            if (user) {
                console.log("Test User 1 saved");
                userService.getApiKey("dagumya", "pas345yet").then(function (apikey) {
                    user1ApiKey = apikey;
                    userService.createUser(sample_susan).then(function (user) {
                        if (user) {
                            console.log("Test User 2 saved");
                            branchService.hardCreate(sample_branch1).then(function (branch) {
                                if (branch) {
                                    console.log("Branch 1 created successfully");
                                    branchService.hardCreate(sample_branch2).then(function (branch) {
                                        if (branch) {
                                            console.log("Second branch created successfully");
                                            userService.hardCreate(sample_employee).then(function (user) {
                                                if(user){
                                                    console.log("Employee created successfully");
                                                    done();
                                                } else {
                                                    console.log("Employee did not create successfully");
                                                    done();
                                                }
                                            }).catch(function (err) {
                                                console.log(err);
                                                done();
                                            })
                                        } else {
                                            console.log("Second branch did not create successfully");
                                            done();
                                        }
                                    }).catch(function (err) {
                                        console.log(err);
                                        done();
                                    })
                                } else {
                                    console.log("Branch 1 was not created successfully");
                                    done();
                                }
                            }).catch(function (err) {
                                console.log(err);
                                done();
                            })
                        } else {
                            console.log("Test User 2 not saved");
                            done();
                        }
                    }).catch(function (err) {
                        console.log(err);
                        done();
                    })
                }).catch(function (err) {
                    console.log(err);
                    done();
                });
            } else {
                console.log("Test User 1 not saved");
                done();
            }
        }).catch(function (err) {
            console.log(err);
            done();
        });

    });

    describe("POST /pet", function () {
        it("POST /pet : should fail to create a new pet because foundby username not in system",
            function(done){
                chai.request(server)
                    .post('/pet?apiKey=' + employeeApiKey)
                    .send(user_input5)
                    .end(function(err, res){
                        res.should.have.status(400);
                        done()
                    })
            });

        it("POST /pet : should fail to create a new pet because branch is full",
            function(done){
                chai.request(server)
                    .post('/pet?apiKey=' + employeeApiKey)
                    .send(user_input3)
                    .end(function(err, res){
                        res.should.have.status(400);
                        done()
                    })
            });
        it("POST /pet : should create a new pet",
            function (done) {
                chai.request(server)
                    .post('/pet?apiKey=' + employeeApiKey)
                    .send(user_input1)
                    .end(function (err, res) {
                        if (err) {
                            return console.log("error" + err.message)
                        }
                        res.should.have.status(201);
                        res.body.should.have.property("microchipId").eql("A010");
                        res.body.should.have.property("name").eql("Popoloski");
                        res.body.should.have.property("species").eql("CAT");
                        res.body.should.have.property("breed").eql("Funny Cats");
                        res.body.should.have.property("gender").eql("FEMALE");
                        res.body.should.have.property("category").eql("SMALL");
                        res.body.should.have.property("weight").eql(10);
                        res.body.should.have.property("age").eql(4);
                        res.body.should.have.property("colorDescription").eql("Jet Black");
                        res.body.should.have.property("personality").eql("Mysterious");
                        res.body.should.have.property("notes").eql("Has scar across eye");
                        res.body.should.have.property("neutered").eql(false);
                        res.body.should.have.property("disabled").eql(false);
                        res.body.should.have.property("imageUrl").eql("/images/test");
                        res.body.should.have.property("foundBy").eql("dagumya");
                        res.body.should.have.property("latitude").eql(41.40338);
                        res.body.should.have.property("longitude").eql(10.4418);
                        res.body.should.have.property("branchId").eql('PC01');
                        done();
                    })
            });
        it("POST /pet : should create a new pet ",
            function (done) {
                chai.request(server)
                    .post('/pet?apiKey=' + employeeApiKey)
                    .send(user_input2)
                    .end(function (err, res) {
                        if (err) {
                            return console.log("error" + err.message)
                        }
                        res.should.have.status(201);
                        res.body.should.have.property("microchipId").eql("A020");
                        res.body.should.have.property("name").eql("Mischief");
                        res.body.should.have.property("species").eql("DOG");
                        res.body.should.have.property("breed").eql("Persian");
                        res.body.should.have.property("gender").eql("MALE");
                        res.body.should.have.property("category").eql("MEDIUM");
                        res.body.should.have.property("weight").eql(15);
                        res.body.should.have.property("age").eql(2);
                        res.body.should.have.property("colorDescription").eql("Brown with white spots");
                        res.body.should.have.property("personality").eql("Very Playful");
                        res.body.should.have.property("notes").eql("Rescued");
                        res.body.should.have.property("neutered").eql(true);
                        res.body.should.have.property("disabled").eql(false);
                        res.body.should.have.property("imageUrl").eql("/images/test1");
                        res.body.should.have.property("foundBy").eql("susang");
                        res.body.should.have.property("latitude").eql(78.40338);
                        res.body.should.have.property("longitude").eql(23.4418);
                        res.body.should.have.property("branchId").eql('PR04');
                        done();
                    })
            });
        it("POST /pet : should fail to save pet details because pet is already saved. Unique microship id",
            function (done) {
                chai.request(server)
                    .post("/pet?apiKey=" + employeeApiKey)
                    .send(user_input1)
                    .end(function (err, res) {
                        res.should.have.status(400);
                        done();
                    })
            });

        it("GET /pet : should retrieve both pets that we just saved",
            function (done) {
                chai.request(server)
                    .get("/pet?apiKey=" + user1ApiKey)
                    .send()
                    .end(function (err, res) {
                        res.should.have.status(200);
                        done();
                    })
            });

        it("GET /pet/:microchipId : should retrieve a pet, given a microchip Id",
            function (done) {
                chai.request(server)
                    .get("/pet/A010?apiKey=" + user1ApiKey)
                    .send()
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.body.should.be.a("object");
                        res.body.should.have.property("microchipId").eql("A010");
                        res.body.should.have.property("name").eql("Popoloski");
                        res.body.should.have.property("species").eql("CAT");
                        res.body.should.have.property("breed").eql("Funny Cats");
                        res.body.should.have.property("gender").eql("FEMALE");
                        res.body.should.have.property("category").eql("SMALL");
                        res.body.should.have.property("weight").eql(10);
                        res.body.should.have.property("age").eql(4);
                        res.body.should.have.property("colorDescription").eql("Jet Black");
                        res.body.should.have.property("personality").eql("Mysterious");
                        res.body.should.have.property("notes").eql("Has scar across eye");
                        res.body.should.have.property("neutered").eql(false);
                        res.body.should.have.property("disabled").eql(false);
                        res.body.should.have.property("imageUrl").eql("/images/test");
                        done()
                    })
            });

        it("GET /pet/microchipId : should fail because the pet by that microchip id does not exist",
            function (done) {
                chai.request(server)
                    .get("/pet/AZSDE?apiKey=" + user1ApiKey)
                    .send()
                    .end(function (err, res) {
                        res.should.have.status(400);
                        done()
                    })
            });

        it("PUT /pet/microchipId : should update a pet record already stored in the system ",
            function (done) {
                chai.request(server)
                    .put("/pet/A010?apiKey=" + employeeApiKey)
                    .send(pet_updates)
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.body.should.be.a("object");
                        // assert on the field that were changed.
                        res.body.should.have.property("name").eql("Tommy");
                        res.body.should.have.property("neutered").eql(true);
                        res.body.should.have.property("breed").eql("RagDoll");
                        done();

                    })
            });


        it("PUT /pet/microchipId : should fail to update pet record because update details are not provided",
            function (done) {
                chai.request(server)
                    .put("/pet/A010?apiKey=" + employeeApiKey)
                    .send({})
                    .end(function (err, res) {
                        res.should.have.status(400);
                        done()
                    })
            });

        it("GET /pet_branch/PC01 : should reflect a change in inventory due to create pet",
            function(done){
                chai.request(server)
                    .get("/pet_branch/PC01?apiKey=" + user1ApiKey)
                    .end(function (err, res ){
                        res.body.inventory[0].should.have.property("category").eql("SMALL");
                        res.body.inventory[0].should.have.property("availability").eql(4);
                        res.body.inventory[0].should.have.property("allocated").eql(11);
                        done()
                    })
            });

        it("GET /pet_branch/PR04 : should reflect a change in inventory due to create pet",
            function(done){
                chai.request(server)
                    .get("/pet_branch/PR04?apiKey=" + user1ApiKey)
                    .end(function (err, res ){
                        res.body.inventory[1].should.have.property("category").eql("MEDIUM");
                        res.body.inventory[1].should.have.property("availability").eql(7);
                        res.body.inventory[1].should.have.property("allocated").eql(3);
                        done()
                    })
            });
        /** Start - Test for Location **/

        it("PUT /transfer_pet?microchipId=*&branchUniqueId=* : should fail for transfer to same branch ",
            function (done) {
                chai.request(server)
                    .put("/transfer_pet?microchipId=A010&branchUniqueId=PC01&apiKey=" + employeeApiKey)
                    .end(function (err, res) {
                        res.should.have.status(400);
                        done();
                    })
            });

        it("PUT /transfer_pet?microchipId=*&branchUniqueId=* : should transfer a pet already stored in the system ",
            function (done) {
                chai.request(server)
                    .put("/transfer_pet?microchipId=A010&branchUniqueId=PR04&apiKey=" + employeeApiKey)
                    .end(function (err, res) {
                        res.should.have.status(200);
                        done();
                    })
            });

        it("GET /pet_branch/PC01 : should reflect a change in inventory due to transfer pet",
            function(done){
                chai.request(server)
                    .get("/pet_branch/PC01?apiKey=" + user1ApiKey)
                    .end(function (err, res ){
                        res.body.inventory[0].should.have.property("category").eql("SMALL");
                        res.body.inventory[0].should.have.property("availability").eql(5);
                        res.body.inventory[0].should.have.property("allocated").eql(10);
                        done()
                    })
            });

        it("GET /pet_branch/PR04 : should reflect a change in inventory due to transfer pet",
            function(done){
                chai.request(server)
                    .get("/pet_branch/PR04?apiKey=" + user1ApiKey)
                    .end(function (err, res ){
                        res.body.inventory[0].should.have.property("category").eql("SMALL");
                        res.body.inventory[0].should.have.property("availability").eql(9);
                        res.body.inventory[0].should.have.property("allocated").eql(11);
                        done()
                    })
            });

        it("PUT /transfer_pet?microchipId=*&branchUniqueId=* : should fail for missing params ",
            function (done) {
                chai.request(server)
                    .put("/transfer_pet?microchipId=A010&apiKey=" + employeeApiKey)
                    .end(function (err, res) {
                        res.should.have.status(400);
                        done();
                    })
            });

        it("PUT /transfer_pet?microchipId=*&branchUniqueId=* : should fail for missing params ",
            function (done) {
                chai.request(server)
                    .put("/transfer_pet?branchUniqueId=PR04&apiKey=" + employeeApiKey)
                    .end(function (err, res) {
                        res.should.have.status(400);
                        done();
                    })
            });


        it("GET /pet/get_locations/:microchipId : get pet's all locations",
            function(done){
                chai.request(server)
                    .get("/pet/get_locations/A010?apiKey=" + user1ApiKey)
                    .end(function (err, res ){
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(2);
                        done()
                    })
            });

        it("GET /pet/get_current_location/:microchipId : get pet's current location",
            function(done){
                chai.request(server)
                    .get("/pet/get_current_location/A010?apiKey=" + user1ApiKey)
                    .end(function (err, res ){
                        res.should.have.status(200);
                        res.body.Branch.should.have.property("branchUniqueId").eql("PR04");
                        done()
                    })
            });

        it("GET /pets_in_branch/:branchUniqueId : get all pets that were/are in branch so far",
            function(done){
                chai.request(server)
                    .get("/pets_in_branch/PC01?apiKey=" + user1ApiKey)
                    .end(function (err, res ){
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        done()
                    })
            });

        it("GET /current_pets_in_branch/:branchUniqueId : get all pets that are in branch now",
            function(done){
                chai.request(server)
                    .get("/current_pets_in_branch/PR04?apiKey=" + user1ApiKey)
                    .end(function (err, res ){
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(2);
                        done()
                    })
            });


        it("PUT /update_pet_status?microchipId=*&status=*&username=* : should not update status of pet to ADOPTED when username of owner is not given",
            function (done) {
                chai.request(server)
                    .put("/update_pet_status?microchipId=A010&status=ADOPTED&apiKey=" + employeeApiKey)
                    .end(function (err, res) {
                        console.log(res.text);
                        res.should.have.status(400);
                        done();
                    })
            });

        it("PUT /update_pet_status?microchipId=*&status=*&username=* : should update status of pet ",
            function (done) {
                chai.request(server)
                    .put("/update_pet_status?microchipId=A010&status=ADOPTED&username=dagumya&apiKey=" + employeeApiKey)
                    .end(function (err, res) {
                        console.log(res.text);
                        res.should.have.status(200);
                        done();
                    })
            });

        it("GET /pet_branch/PR04 : should reflect a change in inventory due to pet status change",
            function(done){
                chai.request(server)
                    .get("/pet_branch/PR04?apiKey=" + user1ApiKey)
                    .end(function (err, res ){
                        res.body.inventory[0].should.have.property("category").eql("SMALL");
                        res.body.inventory[0].should.have.property("availability").eql(10);
                        res.body.inventory[0].should.have.property("allocated").eql(10);
                        done()
                    })
            });

        it("PUT /transfer_pet?microchipId=*&branchUniqueId=* : should fail for transfer pet that is adopted ",
            function (done) {
                chai.request(server)
                    .put("/transfer_pet?microchipId=A010&branchUniqueId=PC01&apiKey=" + employeeApiKey)
                    .end(function (err, res) {
                        console.log(res.text);
                        res.should.have.status(400);
                        done();
                    })
            });

        it("PUT /update_pet_status?microchipId=*&status=*&username=* : should not update status of pet to AVAILABLE from ADOPTED when branch is not given",
            function (done) {
                chai.request(server)
                    .put("/update_pet_status?microchipId=A010&status=AVAILABLE&apiKey=" + employeeApiKey)
                    .end(function (err, res) {
                        console.log(res.text);
                        res.should.have.status(400);
                        done();
                    })
            });

        it("PUT /update_pet_status?microchipId=*&status=*&username=* : should update status of pet ",
            function (done) {
                chai.request(server)
                    .put("/update_pet_status?microchipId=A010&status=AVAILABLE&branchUniqueId=PR04&apiKey=" + employeeApiKey)
                    .end(function (err, res) {
                        console.log(res.text);
                        res.should.have.status(200);
                        done();
                    })
            });

        it("GET /pet_branch/PR04 : should reflect a change in inventory due to pet status change",
            function(done){
                chai.request(server)
                    .get("/pet_branch/PR04?apiKey=" + user1ApiKey)
                    .end(function (err, res ){
                        res.body.inventory[0].should.have.property("category").eql("SMALL");
                        res.body.inventory[0].should.have.property("availability").eql(9);
                        res.body.inventory[0].should.have.property("allocated").eql(11);
                        done()
                    })
            });



        /** End - Test for Location **/

        // TODO Explore the logic of deleting pet and how it affects found status and location
        it("DELETE /pet/microchipId : should delete the pet record", function (done) {
            chai.request(server)
                .delete("/pet/A010?apiKey=" + employeeApiKey)
                .send()
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.have.property("microchipId").eql("A010");
                    done();
                })
        });

        it("DELETE /pet/microchipId : should delete the second pet record",
            function (done) {
                chai.request(server)
                    .delete("/pet/A020?apiKey=" + employeeApiKey)
                    .send()
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.body.should.have.property("microchipId").eql("A020");
                        done();
                    })
            });

        it("DELETE /pet/microchipId : should fail to delete the first pet record because it does not exist anymore",
            function (done) {
                chai.request(server)
                    .delete("/pet/A010?apiKey=" + employeeApiKey)
                    .send()
                    .end(function (err, res) {
                        res.should.have.status(400);
                        done();
                    })
            });

        it("GET /pet_branch/PR04 : should reflect a change in inventory due to delete pet",
            function(done){
                chai.request(server)
                    .get("/pet_branch/PR04?apiKey=" + user1ApiKey)
                    .end(function (err, res ){
                        res.body.inventory[0].should.have.property("category").eql("SMALL");
                        res.body.inventory[0].should.have.property("availability").eql(10);
                        res.body.inventory[0].should.have.property("allocated").eql(10);
                        done()
                    })
            });

        it("GET /pet_branch/PR04 : should reflect a change in inventory due to delete pet",
            function(done){
                chai.request(server)
                    .get("/pet_branch/PR04?apiKey=" + user1ApiKey)
                    .end(function (err, res ){
                        res.body.inventory[1].should.have.property("category").eql("MEDIUM");
                        res.body.inventory[1].should.have.property("availability").eql(8);
                        res.body.inventory[1].should.have.property("allocated").eql(2);
                        done()
                    })
            });

        it("GET /pet/A020/found_status : should fail to retrieve found status because it was deleted",
            function (done) {
                chai.request(server)
                    .get("/pet/A020/found_status?apiKey=" + user1ApiKey)
                    .send()
                    .end(function (err, res) {
                        res.should.have.status(400);
                        done()
                    })
            });
    });

    after(function (done) {
        userService.hardDelete("dagumya").then(function (user) {
                if (user) {
                    console.log("User dagumya was deleted");
                    userService.hardDelete("susang").then(function (user) {
                        if (user) {
                            console.log("User 2 deleted");
                            branchService.hardDelete("PR04").then(function (branch) {
                                if (branch) {
                                    console.log("First branch Deleted");
                                    branchService.hardDelete("PC01").then(function (branch) {
                                        if (branch) {
                                            console.log("Second branch deleted");
                                            userService.hardDelete(sample_employee.username).then(function (user) {
                                                if(user){
                                                    console.log("Employee deleted");
                                                    done();
                                                } else {
                                                    console.log("Employee was not deleted");
                                                    done();
                                                }
                                            }).catch(function (err) {
                                                console.log(err);
                                                done();
                                            })
                                        } else {
                                            console.log("Second branch was not deleted");
                                            done();
                                        }
                                    }).catch(function (err) {
                                        console.log(err);
                                        done();
                                    })
                                } else {
                                    console.log("First branch was not deleted");
                                    done();
                                }
                            }).catch(function (err) {
                                console.log(err);
                                done();
                            })
                        } else {
                            console.log("User 2 was not deleted");
                            done();
                        }
                    }).catch(function (err) {
                        console.log(err);
                        done();
                    })
                } else {
                    console.log("User 1 was not deleted");
                    done();
                }
            }).catch(function (err) {
                console.log(err);
                done();
            });
        }
    );
});
/**
 * Created by shravass on 9/24/17.
 */
module.exports = function (app) {
    // loading model
    require('./model/models.server')(app);

    // loading api
    require('./api/home.api.server')(app);
    require('./api/found.status.api.server')(app);
    require('./api/pet.branch.api.server')(app);
    require('./api/pet.location.api.server')(app);
    require('./api/pet.api.server')(app);
    require('./api/user.api.server')(app);


};
process.env.MONGO_LOCAL = "localhost/cs5500_msd_fall2017";
var chai = require("chai");
var chaiHttp = require('chai-http');
var server = require('../server');
var should = chai.should();

chai.use(chaiHttp);

var userService = require("../services/user.service.server");
var petService = require("../services/pet.service.server");
var branchService = require("../services/pet.branch.service.server");

var sampleUser = {
    "firstName": "Shravanthi",
    "lastName": "Raj",
    "streetAddress": "123 34th Ave North",
    "city": "Seattle",
    "state": "WA",
    "zipcode": 98563,
    "phone": "6735463456",
    "username": "shravraj",
    "password": "1234password",
    "email": "shravraj@gmail.com"
};

var sample_employee = {
    username: "employee1",
    password: "pas345yet",
    apiKey: "dfghfjhhrjg3652",
    firstName: "employee",
    lastName: "employee",
    email: "geod@gmail.com",
    phone: "8964563452",
    streetAddress: "123 Common Street",
    city: "Seattle",
    state: "Washington",
    zipcode: 34521,
    role: ['EMPLOYEE'],
    active: true

};

var employeeApi = "dfghfjhhrjg3652";

var sampleBranch = {
    "name": "Seattle Center",
    "branchUniqueId": "seattleCenter0001",
    "inventory": [
        {
            "category": "SMALL",
            "capacity": 10
        },
        {
            "category": "MEDIUM",
            "capacity": 7
        },
        {
            "category": "LARGE",
            "capacity": 5
        }
    ],
    "noOfStaff": 5,
    "streetAddress": "90, 25th Ave NW",
    "city": "Seattle",
    "state": "WA",
    "zipcode": 98109,
    "contact": "2067584893"

};

var samplePet = {
    "microchipId": "AB1234",
    "name": "Popoloski",
    "species": "CAT",
    "gender": "MALE",
    "breed": "Funny Cats",
    "category": "SMALL",
    "weight": 10,
    "age": 4,
    "colorDescription": "Jet Black",
    "personality": "Mysterious",
    "notes": " Has scar across eye",
    "intakeDate": "1/4/2017",
    "neutered": false,
    "disabled": false,
    "imageUrl": "/images/test",
    "latitude": 47.6206167,
    "longitude": -122.33755080000003,
    "foundBy": "shravraj",
    "unique_branch_id": "seattleCenter0001",
    "status": "AVAILABLE"
};
var userApiKey = "";

before(function (done) {
    console.log("starting before");
    userService.createUser(sampleUser)
        .then(function () {
            console.log("user created");
            return userService.getApiKey(sampleUser.username, sampleUser.password);
        })
        .then(function (api) {
            console.log("user api key found");
            userApiKey = api;
            return branchService.createPetBranch(sampleBranch);
        })
        .then(function () {
            console.log("branch created");
            return userService.hardCreate(sample_employee);
        })
        .then(function () {
            console.log("employee created");
            return petService.createPet(samplePet);
        })
        .then(function () {
            console.log(" Found status test:  Data set up successful ");
            done();
        })
        .catch(function (err) {
            console.log(" Found status test: Data was not created " + err);
            done();
        });
});


it('should not fetch nearby found pets for /foundNearBy POST for a distance longer than 8000', function (done) {
    chai.request(server)
        .post('/foundNearBy?apiKey=' + userApiKey)
        .send({
            "longitude": 80.20459189999997,
            "latitude": 12.9874863
        })
        .end(function (err, res) {
            res.should.have.status(200);
            res.body.should.be.a('array');
            res.body.length.should.be.eql(0);
            done();
        });
});

it('should fetch nearby found pets for /foundNearBy POST', function (done) {
    chai.request(server)
        .post('/foundNearBy?apiKey=' + userApiKey)
        .send({
            "longitude": -122.3453207,
            "latitude": 47.6295946
        })
        .end(function (err, res) {
            res.should.have.status(200);
            res.body.should.be.a('array');
            res.body[0].should.have.property('Pet');
            res.body[0].should.have.property('FoundBy');
            res.body[0].should.have.property('location');
            res.body[0].should.have.property('foundDate');
            done();
        });
});

it('should create error for nearby found pets for /foundNearBy POST for invalid schema', function (done) {
    chai.request(server)
        .post('/foundNearBy?apiKey=' + userApiKey)
        .send({
            "longitude": -122.3453207
        })
        .end(function (err, res) {
            res.should.have.status(400);
            done();
        });
});

it('should get found status details for pet for /pet/:petId/found_status GET', function (done) {
    chai.request(server)
        .get('/pet/AB1234/found_status?apiKey=' + userApiKey)
        .end(function (err, res) {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('Pet');
            res.body.should.have.property('FoundBy');
            res.body.should.have.property('location');
            res.body.should.have.property('foundDate');
            done();
        });
});

it('should not have found status details for wrong pet for /pet/:petId/found_status GET', function (done) {
    chai.request(server)
        .get('/pet/AB123456/found_status?apiKey=' + userApiKey)
        .end(function (err, res) {
            res.should.have.status(400);
            done();
        });
});

it('should update pet found status for /pet/found_status PUT', function (done) {
    chai.request(server)
        .put('/pet/found_status?apiKey=' + employeeApi)
        .send({
            "petId": "AB1234",
            "latitude": 47.6206167,
            "longitude": -122.337550802327,
            "foundBy": "shravraj"
        })
        .end(function (err, res) {
            res.should.have.status(200);
            res.text.should.equal('Record updated successfully');
            done();
        });
});

it('should not update pet found status for invalid pet for /pet/found_status PUT', function (done) {
    chai.request(server)
        .put('/pet/found_status?apiKey=' + employeeApi)
        .send({
            "petId": "AB123456",
            "latitude": 47.6206167,
            "longitude": -122.337550802327,
            "foundBy": "shravraj"
        })
        .end(function (err, res) {
            res.should.have.status(400);
            done();
        });
});

it('should not update pet found status for invalid foundBy for /pet/found_status PUT', function (done) {
    chai.request(server)
        .put('/pet/found_status?apiKey=' + employeeApi)
        .send({
            "petId": "AB1234",
            "latitude": 47.6206167,
            "longitude": -122.337550802327,
            "foundBy": "drra"
        })
        .end(function (err, res) {
            res.should.have.status(400);
            done();
        });
});

it('should not update pet found status for invalid schema for /pet/found_status PUT', function (done) {
    chai.request(server)
        .put('/pet/found_status?apiKey=' + employeeApi)
        .send({
            "petId": "AB1234",
            "latitude": 47.6206167,
            "foundBy": "shrini"
        })
        .end(function (err, res) {
            res.should.have.status(400);
            done();
        });
});

after(function (done) {
    petService.deletePet(samplePet.microchipId)
        .then(function () {
            return branchService.hardDelete(sampleBranch.branchUniqueId);
        })
        .then(function () {
            return userService.hardDelete(sampleUser.username);
        })
        .then(function () {
            return userService.hardDelete(sample_employee.username);
        })
        .then(function () {
            console.log("  after: Found status test: Data deletion successful ");
            done();
        })
        .catch(function (err) {
            console.log("  after: Found status test: Data was not deleted " + err);
            done();
        });
});

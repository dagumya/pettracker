/**
 * Created by dagumya on 11/6/17.
 */
module.exports = function(app){
    var userService = require('../services/user.service.server');

    app.post('/user', createUser);
    app.get('/user/:username', isAnyActiveUser, getUserByUserName);
    app.get('/user', getApiKey);
    app.put('/user/:username', isAdminOrSameUser, updateUser);
    app.put('/updateUserRole', isAdmin, updateUserRole);
    app.delete('/user/:username', isAdminOrSameUser, deleteUser);
    app.put('/user/activate/:username', isAdmin, reactivateUser);

    /**
     * Helper function, Given a Response object as a user. Return a user record to the client with a nicer format and without
     * extra mongo fields such as _id and _v
     * @param res  Response object that will be used to send the client a response
     * @param user User document retrieved from a database
     */
    function filterUser(res, user) {
        "use strict";
        // Removes the version, _id, apiKey, and password from user document
        return res.json({
                firstName: user.firstName,
                lastName: user.lastName,
                streetAddress: user.streetAddress,
                city: user.city,
                state: user.state,
                zipcode: user.zipcode,
                username: user.username,
                email: user.email,
                phone: user.phone,
                role: user.role
        });
    }

    function createUser(req, res){
        if ((Object.keys(req.body).length === 0 ) || !req.body) {
            res.status(400);
            return res.send("Please provide user details to register user")
        }
        var user = req.body;
        userService.createUser(user).then(function(user){
                res.status(201);
                return filterUser(res,user)
        }).catch(function(err){
            console.log(err);
            res.status(400);
            res.send(err)
        })
    }

    function getUserByUserName(req, res){
        if (req.params.username){
            var username = req.params.username;
            userService.findUserByUserName(username).then(function(user){
                if (user) {
                    res.status(200);
                    return filterUser(res, user);
                }
            }).catch(function(err){
                res.status(400);
                return res.send(err);
            })
        }else{
            res.status(400);
            return res.send("Please provide a username for user details")
        }

    }

    function getApiKey(req, res){
        if (req.query.username && req.query.password){
            var username = req.query.username;
            var password = req.query.password;
            userService.getApiKey(username, password).then(function(api_key){
                    res.status(200);
                    return res.json({api_key : api_key});
            }).catch(function(err){
                console.log(err);
                res.status(400);
                return res.send("Unable to get API key " + err)
            })
        }else{
            res.status(400);
            res.send("Please provide a username and password")
        }

    }

    function updateUser(req, res){
        if (req.params.username && req.body){
            var username = req.params.username;
            var user = req.body;
            userService.updateUser(username, user).then(function(user){
                    res.status(200);
                    return filterUser(res,user)
            }).catch(function(err){
                res.status(400);
                return res.send(err)
            })
        }else{
            res.status(400);
            res.send("Please provide a username for the user you want to update as well as " +
                "user details")
        }
    }

    function updateUserRole(req, res){
        if (req.query.operation && req.query.username && req.query.role){
            var operation = req.query.operation; // operation user can perform
            var username = req.query.username; //user for admin to update
            var newRole = req.query.role; //new role to assign
            userService.updateUserRole(operation,username, newRole).then(function(user){
                    res.status(200);
                    return filterUser(res,user);
            }).catch(function(err){
                res.status(400);
                res.send(err);
            })
        } else {
            res.status(400).send(" Mandatory query parameters are missing")
        }
    }

    function deleteUser(req, res){
        if (req.params.username){
            var username = req.params.username;
            userService.deleteUser(username).then(function(user){
                    res.status(200);
                    return filterUser(res,user)
            }).catch(function(err){
                console.log(err);
                res.status(400);
                return res.send(err) //could be already deleted or incorrect username
            })

        }
        else{
            res.status(400);
            return res.send("Please provide a username for the user you are trying to delete")
        }
    }

    /**
     * Allows user to reactivate user with user_id
     * @param req
     * @param res
     */
    function reactivateUser(req, res) {
        var username = req.params.username;
        userService.reactivateUser(username).then(function (user) {
            if (user) {
                res.statusMessage = "user successfully reactivated";
                res.status(200);
                return filterUser(res, user)
            }
        }, function (err) {
            res.status(400).send(err);
        }) ;
    }

    function isAdmin(req, res, next) {
        userService.findUserByApiKey(req.query.apiKey).then(function (user) {
            if (user) {
                //make sure user trying to make changes has admin rights
                if (user.role.indexOf('ADMIN') !== -1) {
                    next();
                }
                else {
                    res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
                }
            }
            else {
                res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
            }
        }, function (err) {
            res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
        });
    }

    function isAnyActiveUser(req, res, next) {
        userService.findUserByApiKey(req.query.apiKey).then(function (user) {
            if (user) {
                next();
            }
            else {
                res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
            }
        }, function (err) {
            res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
        });
    }

    function isAdminOrSameUser(req, res, next) {
        userService.findUserByApiKey(req.query.apiKey).then(function (user) {
            if (user) {
                //make sure user trying to make changes has admin rights or same user as user to be changed
                if (user.role.indexOf('ADMIN') !== -1) {
                    next();
                } else if(req.params.username === user.username){
                    next();
                }
                else {
                    res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
                }
            }
            else {
                res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
            }
        }, function (err) {
            res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
        });
    }
};
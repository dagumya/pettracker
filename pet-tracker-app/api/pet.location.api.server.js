/**
 * Created by david on 9/30/17.
 */
"use strict";

module.exports = function (app) {

    /**
     * ROUTES
     * Based on pet route because each pet has a location <parent child relationship>
     */
    app.get('/pet/get_locations/:microchipId', isAnyActiveUser, get_PetLocations);
    app.get('/pet/get_current_location/:microchipId', isAnyActiveUser, getPetCurrentLocation);
    app.get('/pets_in_branch/:branchUniqueId', isAnyActiveUser, get_PetsInBranch);
    app.get('/current_pets_in_branch/:branchUniqueId', isAnyActiveUser, getCurrentPetsInBranch);
    app.put('/transfer_pet', isAdminOrEmployee, transferPet);
    app.put('/update_pet_status', isAdminOrEmployee, updatePetStatus);


    // getting services
    var petLocationService = require('../services/pet.location.service.server');
    var userService = require('../services/user.service.server');


    /**
     *  ROUTE FUNCTIONS
     */

    /**
     * Given a microchip id unique to a pet retrieve all locations related to that microchipId
     * @param req -> Request object
     * @param res -> Response object
     */
    function get_PetLocations(req, res) {
        var microchipId = req.params.microchipId;
        petLocationService.get_PetLocations(microchipId)
            .then(function (locationStatus) {
                res.statusMessage = "Find successful";
                res.status(200).send(locationStatus);
            }, function (err) {
                res.status(400).send(err);
            })
    }

    /**
     * Given a microchip id unique to a pet retrieve current location related to that microchipId
     * @param req -> Request object
     * @param res -> Response object
     */
    function getPetCurrentLocation(req, res) {
        var microchipId = req.params.microchipId;
        petLocationService.getPetCurrentLocation(microchipId)
            .then(function (locationStatus) {
                res.statusMessage = "Find successful";
                res.status(200).send(locationStatus);
            }, function (err) {
                res.status(400).send(err);
            })
    }

    /**
     * Given a branch Id return all the pets with the branch with the id specified.
     * @param req -> Request object
     * @param res -> Response object
     */
    function get_PetsInBranch(req, res) {
        var branchId = req.params.branchUniqueId;
        petLocationService.get_PetsInBranch(branchId)
            .then(function (pets) {
                res.statusMessage = "Find successful";
                res.status(200).send(pets);
            }, function (err) {
                res.status(400).send(err);
            })

    }

    /**
     * Given a branch Id return current pets with the branch with the id specified.
     * @param req -> Request object
     * @param res -> Response object
     */
    function getCurrentPetsInBranch(req, res) {
        var branchId = req.params.branchUniqueId;
        petLocationService.getCurrentPetsInBranch(branchId)
            .then(function (pets) {
                res.statusMessage = "Find successful";
                res.status(200).send(pets);
            }, function (err) {
                res.status(400).send(err);
            })

    }


    function transferPet(req, res) {
        var petId = req.query.microchipId;
        var branchId = req.query.branchUniqueId;
        if (petId && branchId) {
            petLocationService.update_location(petId, branchId).then(function (loc) {
                res.status(200).send("Pet has been transferred successfully");
            }, function (err) {
                res.status(400).send(err);
            })
        } else {
            res.status(400).send(" Mandatory query parameters are missing")
        }
    }

    function updatePetStatus(req, res) {
        var petId = req.query.microchipId;
        var petStatus = req.query.status;
        var ownerUserName = req.query.username;
        var branchUniqueId = req.query.branchUniqueId;
        if (petId && petStatus) {
            petLocationService.updatePetStatus(petId, petStatus, ownerUserName, branchUniqueId).then(function (loc) {
                res.status(200).send("Pet status been updated successfully");
            }, function (err) {
                res.status(400).send(err);
            })
        } else {
            res.status(400).send(" Mandatory query parameters are missing")
        }
    }

    function isAdminOrEmployee(req, res, next) {
        userService.findUserByApiKey(req.query.apiKey).then(function (user) {
            if (user) {
                //make sure user trying to make changes has admin/employee rights
                if (user.role.indexOf('ADMIN') !== -1 || user.role.indexOf('EMPLOYEE') !== -1) {
                    next();
                }
                else {
                    res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
                }
            }
            else {
                res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
            }
        }, function (err) {
            res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
        });
    }

    function isAnyActiveUser(req, res, next) {
        userService.findUserByApiKey(req.query.apiKey).then(function (user) {
            if (user) {
                next();
            }
            else {
                res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
            }
        }, function (err) {
            res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
        });
    }

};

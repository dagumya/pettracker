process.env.MONGO_LOCAL = "localhost/cs5500_msd_fall2017";
var chai = require("chai");
var chaiHttp = require('chai-http');
var server = require('../server');
var should = chai.should();

chai.use(chaiHttp);

/**
 * IMPORT MODELS TO SAVE TEST DATA BEFORE RUNNING TESTS
 */
var branchService = require("../services/pet.branch.service.server");
var userService = require("../services/user.service.server");

var sample_employee = {
    username: "employee",
    password: "pas345yet",
    apiKey: "dfsghfjds23423",
    firstName: "employee",
    lastName: "employee",
    email: "geod@gmail.com",
    phone: "8964563452",
    streetAddress: "123 Common Street",
    city: "Seattle",
    state: "Washington",
    zipcode: 34521,
    role: ['EMPLOYEE'],
    active: true

};

var sample_user = {
    username: "user1",
    password: "pas345yet",
    apiKey: "hgfhftyurrty56",
    firstName: "user",
    lastName: "user",
    email: "geod@gmail.com",
    phone: "8964563452",
    streetAddress: "123 Common Street",
    city: "Seattle",
    state: "Washington",
    zipcode: 34521,
    role: ['USER'],
    active: true

};

var sample_admin = {
    username: "admin3",
    password: "pas345yet",
    apiKey: "uj9567gnre",
    firstName: "admin",
    lastName: "admin",
    email: "geod@gmail.com",
    phone: "8964563452",
    streetAddress: "123 Common Street",
    city: "Seattle",
    state: "Washington",
    zipcode: 34521,
    role: ['ADMIN'],
    active: true

};

var employeeApiKey = "dfsghfjds23423";
var userApiKey = "hgfhftyurrty56";
var adminApiKey = "uj9567gnre";

    describe("Test Pet Branch Routes", function() {

    before(function (done) {
            userService.hardCreate(sample_employee)
                .then(function (user) {
                    if (user) {
                        console.log("Employee was created");
                        return userService.hardCreate(sample_user);
                    } else {
                        console.log("Employee was not created");
                        done();
                    }
                })
                .then(function (user) {
                    if(user){
                        console.log("User was created");
                        return userService.hardCreate(sample_admin);
                    } else {
                        console.log("User was not created");
                        done();
                    }
                })
                .then(function (user) {
                    if(user){
                        console.log("Admin was created");
                        done();
                    } else {
                        console.log("Admin was not created");
                        done();
                    }
                })
                .catch(function (err) {
                console.log(err);
                done();
            });
        }
    );

    it('should create new pet branch for /pet_branch POST', function (done) {
        chai.request(server)
            .post('/pet_branch?apiKey=' + adminApiKey)
            .send({
                name: 'Swedish Center',
                branchUniqueId: 'swe2sea',
                inventory: [
                    {
                        category: 'SMALL',
                        capacity: 20
                    },
                    {
                        category: 'MEDIUM',
                        capacity: 15
                    },
                    {
                        category: 'LARGE',
                        capacity: 10
                    }
                ],
                noOfStaff: 10,
                streetAddress: '90, Aloha Ave',
                city: 'Seattle',
                state: 'WA',
                zipcode: 98134,
                contact: '2067589833'

            })
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('branchUniqueId').eql('swe2sea');
                res.body.should.have.property('name').eql('Swedish Center');
                res.body.should.have.property('noOfStaff').eql(10);
                res.body.should.have.property('streetAddress').eql('90, Aloha Ave');
                res.body.should.have.property('city').eql('Seattle');
                res.body.should.have.property('inventory');
                res.body.inventory.should.be.a('array');
                res.body.inventory[0].should.have.property('capacity');
                done();
            });
    });


    it('should return 405 (incorrect json syntax) for /pet_branch POST', function (done) {
        chai.request(server)
            .post('/pet_branch?apiKey=' + adminApiKey)
            .send({
                name: 'Swedish Center',
                branchUniqueId: 'swe2sea',
                noOfStaff: 10,
                streetAddress: '90, Aloha Ave',
                city: 'Seattle',
                state: 'WA',
                zipcode: 98134,
                contact: '2067589833'

            })
            .end(function (err, res) {
                res.should.have.status(405);
                done();
            });
    });

    it('should return 405 for already existing pet branch for /pet_branch POST', function (done) {
        chai.request(server)
            .post('/pet_branch?apiKey=' + adminApiKey)
            .send({
                name: 'Swedish Center',
                branchUniqueId: 'swe2sea',
                inventory: [
                    {
                        category: 'SMALL',
                        capacity: 20
                    },
                    {
                        category: 'MEDIUM',
                        capacity: 15
                    },
                    {
                        category: 'LARGE',
                        capacity: 10
                    }
                ],
                noOfStaff: 10,
                streetAddress: '90, Aloha Ave',
                city: 'Seattle',
                state: 'WA',
                zipcode: 98134,
                contact: '2067589833'

            })
            .end(function (err, res) {
                res.should.have.status(405);
                res.text.should.equal(' PetBranch details already exist ');
                done();
            });
    });

        it('should return 405 for duplicate inventory info for /pet_branch POST', function (done) {
            chai.request(server)
                .post('/pet_branch?apiKey=' + adminApiKey)
                .send({
                    name: 'Swedish Center',
                    branchUniqueId: 'swe2sea2',
                    inventory: [
                        {
                            category: 'SMALL',
                            capacity: 20
                        },
                        {
                            category: 'SMALL',
                            capacity: 15
                        },
                        {
                            category: 'LARGE',
                            capacity: 10
                        }
                    ],
                    noOfStaff: 10,
                    streetAddress: '90, Aloha Ave',
                    city: 'Seattle',
                    state: 'WA',
                    zipcode: 98134,
                    contact: '2067589833'

                })
                .end(function (err, res) {
                    res.should.have.status(405);
                    res.text.should.equal(' Duplicate inventory for same category SMALL');
                    done();
                });
        });

    it('should list ALL branches on /pet_branches GET', function (done) {
        chai.request(server)
            .get('/pet_branches?apiKey=' + userApiKey)
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body[0].should.have.property('branchUniqueId');
                res.body[0].should.have.property('name');
                res.body[0].should.have.property('noOfStaff');
                res.body[0].should.have.property('streetAddress');
                res.body[0].should.have.property('city');
                res.body[0].should.have.property('inventory');
                res.body[0].inventory.should.be.a('array');
                res.body[0].inventory[0].should.have.property('capacity');
                done();
            });
    });

    it('should list ALL branches that have availability for small animals on /pet_branch?category=SMALL GET', function (done) {
        chai.request(server)
            .get('/pet_branch?category=SMALL&apiKey=' + userApiKey)
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                done();
            });
    });

    it('should list ALL branches that have availability for medium animals on /pet_branch?category=MEDIUM GET', function (done) {
        chai.request(server)
            .get('/pet_branch?category=MEDIUM&apiKey=' + userApiKey)
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                done();
            });
    });

    it('should list ALL branches that have availability for large animals on /pet_branch?category=LARGE GET', function (done) {
        chai.request(server)
            .get('/pet_branch?category=LARGE&apiKey=' + userApiKey)
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                done();
            });
    });

    it('should return 405 for invalid category on /pet_branch?category=invalid GET', function (done) {
        chai.request(server)
            .get('/pet_branch?category=invalid&apiKey=' + userApiKey)
            .end(function (err, res) {
                res.should.have.status(405);
                done();
            });
    });

        it('should return 405 for no category specified on /pet_branch? GET', function (done) {
            chai.request(server)
                .get('/pet_branch?apiKey=' + userApiKey)
                .end(function (err, res) {
                    res.should.have.status(405);
                    done();
                });
        });

    it('should get branch that has specified branchUniqueId /pet_branch/sea1sea GET', function (done) {
        chai.request(server)
            .get('/pet_branch/swe2sea?apiKey=' + userApiKey)
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('branchUniqueId').eql('swe2sea');
                res.body.should.have.property('name').eql('Swedish Center');
                res.body.should.have.property('noOfStaff').eql(10);
                res.body.should.have.property('streetAddress').eql('90, Aloha Ave');
                res.body.should.have.property('city').eql('Seattle');
                res.body.should.have.property('inventory');
                res.body.inventory.should.be.a('array');
                res.body.inventory[0].should.have.property('capacity');
                done();
            });
    });


    it('should return 400 if specified branchUniqueId id not present for /pet_branch/seattle GET', function (done) {
        chai.request(server)
            .get('/pet_branch/seattle?apiKey=' + userApiKey)
            .end(function (err, res) {
                res.should.have.status(400);
                done();
            });
    });

    it('should update pet branch for /pet_branch PUT', function (done) {
        chai.request(server)
            .put('/pet_branch?apiKey=' + employeeApiKey)
            .send({
                name: 'Swedish Center',
                branchUniqueId: 'swe2sea',
                inventory: [
                    {
                        category: 'SMALL',
                        capacity: 10
                    },
                    {
                        category: 'MEDIUM',
                        capacity: 15
                    },
                    {
                        category: 'LARGE',
                        capacity: 10
                    }
                ],
                noOfStaff: 10,
                streetAddress: '90, Aloha Ave',
                city: 'Seattle',
                state: 'WA',
                zipcode: 98134,
                contact: '2067589833'

            })
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('branchUniqueId').eql('swe2sea');
                res.body.should.have.property('name').eql('Swedish Center');
                res.body.should.have.property('noOfStaff').eql(10);
                res.body.should.have.property('streetAddress').eql('90, Aloha Ave');
                res.body.should.have.property('city').eql('Seattle');
                res.body.should.have.property('inventory');
                res.body.inventory.should.be.a('array');
                res.body.inventory[2].should.have.property('capacity').eql(10);
                done();
            });
    });

    it('should return 405 for invalid branch unique id for /pet_branch PUT', function (done) {
        chai.request(server)
            .put('/pet_branch?apiKey=' + employeeApiKey)
            .send({
                name: 'Swedish Center',
                branchUniqueId: 'swedish',
                inventory: [
                    {
                        category: 'SMALL',
                        capacity: 20
                    },
                    {
                        category: 'MEDIUM',
                        capacity: 15
                    },
                    {
                        category: 'LARGE',
                        capacity: 10
                    }
                ],
                noOfStaff: 10,
                streetAddress: '90, Aloha Ave',
                city: 'Seattle',
                state: 'WA',
                zipcode: 98134,
                contact: '2067589833'

            })
            .end(function (err, res) {
                res.should.have.status(405);
                done();
            });
    });

    it('should return 405 for invalid pet branch json for /pet_branch PUT', function (done) {
        chai.request(server)
            .put('/pet_branch?apiKey=' + employeeApiKey)
            .end(function (err, res) {
                res.should.have.status(405);
                done();
            });
    });


    it('should delete pet branch json for /pet_branch DELETE', function (done) {
        chai.request(server)
            .delete('/pet_branch/swe2sea?apiKey=' + adminApiKey)
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('branchUniqueId').eql('swe2sea');
                res.body.should.have.property('name').eql('Swedish Center');
                res.body.should.have.property('noOfStaff').eql(10);
                res.body.should.have.property('streetAddress').eql('90, Aloha Ave');
                res.body.should.have.property('city').eql('Seattle');
                res.body.should.have.property('inventory');
                res.body.inventory.should.be.a('array');
                res.body.should.have.property('active').eql(false);
                done();
            });
    });

    it('should return 400 for invalid pet branch json for /pet_branch DELETE', function (done) {
        chai.request(server)
            .delete('/pet_branch/123?apiKey=' + adminApiKey)
            .end(function (err, res) {
                res.should.have.status(400);
                res.text.should.equal(' PetBranch could not be deleted:  Branch details could not be found ');
                done();
            });
    });

        it('should activate pet branch json for /activateBranch/:branchUniqueId PUT', function (done) {
            chai.request(server)
                .put('/activateBranch/swe2sea?apiKey=' + adminApiKey)
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('branchUniqueId').eql('swe2sea');
                    res.body.should.have.property('name').eql('Swedish Center');
                    res.body.should.have.property('noOfStaff').eql(10);
                    res.body.should.have.property('streetAddress').eql('90, Aloha Ave');
                    res.body.should.have.property('city').eql('Seattle');
                    res.body.should.have.property('inventory');
                    res.body.inventory.should.be.a('array');
                    res.body.should.have.property('active').eql(true);
                    done();
                });
        });

        it('should return 400 for invalid pet branch json for /activateBranch/:branchUniqueId PUT', function (done) {
            chai.request(server)
                .put('/activateBranch/123?apiKey=' + adminApiKey)
                .end(function (err, res) {
                    res.should.have.status(400);
                    done();
                });
        });

    after(function (done) {
            branchService.hardDelete("swe2sea").then(function (branch) {
                if (branch) {
                    console.log("First branch Deleted");
                    return userService.hardDelete(sample_user.username)
                } else {
                    console.log("First branch was not deleted");
                    done();
                }
            }).then(function (user) {
                if (user) {
                    console.log("User was deleted");
                    return userService.hardDelete(sample_employee.username)
                } else {
                    console.log("User was not deleted");
                    done();
                }
            }).then(function (user) {
                if (user) {
                    console.log("Employee was deleted");
                    return userService.hardDelete(sample_admin.username)
                } else {
                    console.log("Employee was not deleted");
                    done();
                }
            }).then(function (user) {
                if (user) {
                    console.log("Admin was deleted");
                    done();
                } else {
                    console.log("Admin was not deleted");
                    done();
                }
            }).catch(function (err) {
                console.log("Caught an error: " + err);
                done();
            })
        }
    );
});

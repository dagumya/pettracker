// import libraries
var Validator = require('jsonschema').Validator;
var petValidator = new Validator();
var q = require('q');

// Pet schema defined for validation
var userInputSchema = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "petCreationUserInputSchema",
    "type": "object",
    "properties": {
        "microchipId": {"type": "string"},
        "name": {"type": "string"},
        "species": {"type": "string", "enum": ["DOG", "CAT"]},
        "gender": {"type": "string", "enum": ["MALE", "FEMALE"]},
        "breed": {"type": "string"},
        "category": {"type": "string", "enum": ["SMALL", "MEDIUM", "LARGE"]},
        "weight": {"type": "integer"},
        "age": {"type": "integer"},
        "colorDescription": {"type": "string"},
        "personality": {"type": "string"},
        "notes": {"type": "string"},
        "intakeDate": {"type": "string", "format": "date-time"},
        "neutered": {"type": "boolean"},
        "disabled": {"type": "boolean"},
        "imageUrl": {"type": "string"},
        "latitude": {"type": "number"}, // for found status
        "longitude": {"type": "number"}, // for found status
        "foundBy": {"type": "string"}, // for found status
        "unique_branch_id": {"type": "string"},
        "status": {
            "description": "Current status of the pet at the current location",
            "type": "string",
            "enum": ["AVAILABLE", "UNAVAILABLE"]
        }
    },
    "additionalProperties": false,
    "required": ["microchipId", "name", "species", "gender", "breed", "category", "weight",
        "age", "colorDescription", "imageUrl", "latitude", "longitude", "foundBy",
        "unique_branch_id", "status"]
};
var petSchema = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Pet",
    "description": "Pet details",
    "type": "object",
    "properties": {
        "microchipID": {"type": "string"},
        "name": {"type": "string"},
        "species": {"type": "string", "enum": ["DOG", "CAT"]},
        "gender": {"type": "string", "enum": ["MALE", "FEMALE"]},
        "breed": {"type": "string"},
        "category": {"type": "string", "enum": ["SMALL", "MEDIUM", "LARGE"]},
        "weight": {"type": "integer"},
        "age": {"type": "integer"},
        "colorDescription": {"type": "string"},
        "personality": {"type": "string"},
        "notes": {"type": "string"},
        "intakeDate": {"type": "string", "format": "date-time"},
        "neutered": {"type": "boolean"},
        "disabled": {"type": "boolean"},
        "imageUrl": {"type": "string"}
    },
    "additionalProperties": false,
    "required": ["microchipID", "name", "species", "gender", "breed", "weight", "age", "colorDescription", "imageUrl"]
};

// import methods from other services
var branchService = require("../services/pet.branch.service.server");
var foundService = require("../services/found.status.service.server");
var locationService = require("../services/pet.location.service.server");
var userService = require("../services/user.service.server");

// getting models
var petModel = require('../model/pet/pet.model.server');

/**
 * Updates Pet referenced by microchip Id
 * @param microchipId :  pet you want to update
 * @param update_details : new pet details
 * @returns {*|promise}
 */
function updatePet(microchipId, update_details) {
    var defer = q.defer();
    petModel.updatePet(microchipId, update_details)
        .then(function (updated_pet) {
            if (updated_pet) {
                delete updated_pet._doc._id;
                delete updated_pet._doc.__v;
                defer.resolve(updated_pet)
            } else {
                defer.reject("Pet referenced by id does not exist")
            }
        }).catch(function (err) {
        defer.reject(err)
    });
    return defer.promise

}

/**
 * Delete Pet referenced by microchip Id
 * @param microchipId : pet to delete
 * @returns {*|promise}
 */
function deletePet(microchipId) {
    var defer = q.defer();
    petModel.findPetByID(microchipId).then(function (pet) {
        if (pet) {
            decreaseInventory(microchipId).then(function (branch) {
                return foundService.deleteFoundStatus(microchipId);
            }).then(function (found_status) {
                return locationService.deleteLocationsByPet(microchipId);
            }).then(function (location) {
                return petModel.deletePet(microchipId)
            }).then(function (pet) {
                delete pet._doc._id;
                delete pet._doc.__v;
                defer.resolve(pet)
            }).catch(function (err) {
                defer.reject(err)
            });
        } else {
            defer.reject("Pet not found")
        }
    }).catch(function (err) {
        defer.reject(err)
    });

    return defer.promise
}

/**
 * Given a microchip Id, find pet category and current branch location. Reduce the inventory in branch to
 * reflect deleting on pet from system
 * @param microchipId
 * @returns {*|promise}
 */
function decreaseInventory(microchipId) {
    var defer = q.defer();
    petModel.findPetByID(microchipId).then(function (pet) {
        if (pet) {
            locationService.getPetCurrentLocation(microchipId).then(function (location) {
                // current location need not be present
                if(location){
                    var inv_details = {
                        branchUniqueId: location.Branch.branchUniqueId,
                        category: pet.category,
                        noOfPetsAllocated: -1
                    };
                    return branchService.updateInventoryDetails(inv_details)
                }
            }).then(function (branch) {
                defer.resolve(branch)
            }).catch(function (err) {
                defer.reject(err)
            });
        }
    }).catch(function (err) {
        defer.reject(err)
    });

    return defer.promise
}


/**
 * Returns all pets registered in pet tracking system.
 * @returns {*|promise}  if fulfilled return pets registered in system
 * @returns {*|promise} if rejected return err
 */
function getAllPets() {
    var defer = q.defer();
    petModel.findAllPets()
        .then(function (pets) {
            defer.resolve(pets)
        }).catch(function (err) {
        defer.reject(err)
    });
    return defer.promise
}

/**
 * Retrieve pet given a pet microchip Id
 * @param microchipId : pet you wish to retrieve
 * @returns {*|promise}
 */
function findPetByMicrochipID(microchipId) {
    var defer = q.defer();
    petModel.findPetByID(microchipId).then(function (pet) {
        if (pet) {
            defer.resolve(pet);
        } else {
            defer.reject("Pet is not saved in the system");
        }
    }).catch(function (err) {
        console.log(err);
        defer.reject(err);
    });
    return defer.promise;
}

/**
 * Validate the original user input with pet details found status details and location details
 * @param user_input_obj : Object with pet relevant details to save.
 * @returns {*|promise}
 */
function validateUserInput(user_input_obj) {
    var defer = q.defer();
    var schemaValidation = petValidator.validate(user_input_obj, userInputSchema);
    if (schemaValidation.valid) {

        // checking status with owner
        if (user_input_obj.status === 'AVAILABLE' || user_input_obj.status === 'UNAVAILABLE' || user_input_obj.status === 'DECEASED') {
            if (user_input_obj.owner_username) {
                defer.reject(' Invalid combination of status and owner ');
            }
        }
        defer.resolve(user_input_obj);
    } else {
        console.log(schemaValidation.errors);
        defer.reject(schemaValidation.errors)
    }
    return defer.promise
}

/**
 * Given the original user input with pet, found status and location details. Use pet model to create a pet
 * record in database
 * @param user_input : Object with pet details, found status and location
 * @returns {*|promise}
 */
function storeInPetModel(user_input) {
    var defer = q.defer();
    // check that pet is not already saved
    petModel.findPetByID(user_input.microchipId).then(function (pet) {
        if (pet) {
            defer.reject("Pet details already exists");
        } else {
            var pet_details = {
                microchipId: user_input.microchipId,
                name: user_input.name,
                species: user_input.species,
                gender: user_input.gender,
                breed: user_input.breed,
                category: user_input.category,
                weight: user_input.weight,
                age: user_input.age,
                colorDescription: user_input.colorDescription,
                personality: user_input.personality,
                notes: user_input.notes,
                intakeDate: user_input.intakeDate,
                neutered: user_input.neutered,
                disabled: user_input.disabled,
                imageUrl: user_input.imageUrl
            };
            petModel.create(pet_details, function (err, saved_pet) {
                if (err) {
                    defer.reject(err);
                }
                if (saved_pet) {
                    user_input.petId = saved_pet._id;
                    defer.resolve(user_input);
                } else {
                    defer.reject("Unable to save pet");
                }
            });
        }
    }, function (err) {
        defer.reject(err);
    });
    return defer.promise; // important to put this return statement here otherwise promises wont work
}

/**
 * After storing the pet details, this function extracts the relevant details for a pet's found status
 * and stores the pets found status details
 * @param from_pet_save : Object with pet found status and location details for a pet
 * @returns {*|promise}
 */
function storeInFoundStatusModel(from_pet_save) {
    var defer = q.defer();
    var found_details = {
        petId: from_pet_save.microchipId, // pass microchip id .. not actual petId
        latitude: from_pet_save.latitude,
        longitude: from_pet_save.longitude,
        foundBy: from_pet_save.foundBy
    };

    foundService.createFoundStatus(found_details).then(function (saved_found_status) {
        from_pet_save.foundDate = saved_found_status.foundDate;
        defer.resolve(from_pet_save);
    }, function (err) {
        defer.reject(err);
    });
    return defer.promise;
}

/**
 * After storing the found status details, this function extract the relevant details for a pet's location
 * and stores the pet's location details
 * @param from_found_save : Object with pet found status and location details for a pet
 * @returns {*|promise}
 */
function storeInLocation(from_found_save) {
    var defer = q.defer();
    var loc_rec = {
        microchipId: from_found_save.microchipId,
        status: from_found_save.status,
        branchId: from_found_save.unique_branch_id
    };
    if (from_found_save.hasOwnProperty('owner_username')) {
        loc_rec.owner_username = from_found_save.owner_username
    }
    locationService.create_location(loc_rec)
        .then(function (loc) {
            // location has been created
            from_found_save.startDate = loc.startDate;
            defer.resolve(from_found_save)
        }).catch(function (err) {
        defer.reject(err)
    });
    return defer.promise
}

/**
 * After storing the location details, this function etracts the branch id and category so that the
 * corresponding branch record inventory can be updated to reflect the pet staying at that branch.
 * @param from_loc_save : Object with pet found status and location details for a pet
 * @returns {*|promise}
 */
function adjustInventory(from_loc_save) {
    var defer = q.defer();
    var required_parameters = {
        branchUniqueId: from_loc_save.unique_branch_id,
        category: from_loc_save.category,
        noOfPetsAllocated: 1
    };
    branchService.updateInventoryDetails(required_parameters).then(function (branch) {
        defer.resolve(from_loc_save)
    }).catch(function (err) {
        defer.reject(err)
    });

    return defer.promise
}
/**
 * PRIVATE HELPER FUNCTION : Given user input, validate that found by username, owner username if available
 * are registered in the system. Also ensure that branch has capacity to add pet in the category given in
 * the user input -> Checking this before insertion ensure no errors in pet creation chaining
 * @param user_input : user given input for create pet
 * @returns {*|promise}
 */
function dependencies(user_input) {
    var defer = q.defer();
    verifyBranchandAvailability(user_input.unique_branch_id, user_input.category)
        .then(function () {
            verifyUser(user_input.foundBy)
                .then(function () {
                    if (user_input.owner_username) {
                        verifyUser(user_input.owner_username)
                            .then(function () {
                                defer.resolve()
                            }).catch(function (err) {
                            defer.reject(err)
                        })
                    } else {
                        defer.resolve()
                    }
                }).catch(function (err) {
                defer.reject(err)
            });
        }).catch(function (err) {
        defer.reject(err)
    });
    return defer.promise
}

/**
 * PRIVATE HELPER FUNCTION : Given a username, verify that the user is registered in the system
 * @param username
 * @returns {*|promise}
 */
function verifyUser(username) {
    var defer = q.defer();
    userService.findUserByUserName(username)
        .then(function (user) {
            defer.resolve()
        }).catch(function (err) {
        defer.reject(err)
    });
    return defer.promise
}
/**
 * PRIVATE HELPER FUNCTION : Given a branch id and category, verify that branch has capacity to add pet
 * of that category
 * @param branch : Branch Id
 * @param category : pet you wish to insert
 * @returns {*|promise}
 */
function verifyBranchandAvailability(branch, category) {
    var defer = q.defer();
    branchService.findPetBranch(branch)
        .then(function (branch) {
            var categories = ["SMALL", "MEDIUM", "LARGE"];
            var inventory_obj = branch.inventory[categories.indexOf(category)];
            if (inventory_obj.availability > 0) {
                defer.resolve()
            } else {
                defer.reject("Unable to locate branch here. At Full Capacity")
            }
        }).catch(function (err) {
        defer.reject(err)
    });
    return defer.promise;
}

/**
 *  Filter the final response we will send to the user at the end of the pet record creation process.
 * @param from_location_save : Object with pet found status and location details for a pet
 * @returns {*|promise}
 */
function filterResponse(from_location_save) {
    "use strict";
    var defer = q.defer();
    var userResponse = {
            microchipId: from_location_save.microchipId,
            name: from_location_save.name,
            species: from_location_save.species,
            breed: from_location_save.breed,
            gender: from_location_save.gender,
            category: from_location_save.category,
            weight: from_location_save.weight,
            age: from_location_save.age,
            colorDescription: from_location_save.colorDescription,
            personality: from_location_save.personality,
            notes: from_location_save.notes,
            intakeDate: from_location_save.intakeDate,
            neutered: from_location_save.neutered,
            disabled: from_location_save.disabled,
            imageUrl: from_location_save.imageUrl,
            latitude: from_location_save.latitude,
            longitude: from_location_save.longitude,
            foundBy: from_location_save.foundBy,
            foundDate: from_location_save.foundDate,
            branchId: from_location_save.unique_branch_id
    };
    if (from_location_save.hasOwnProperty("owner_username")) {
        userResponse.owner = from_location_save.owner_username
    }
    defer.resolve(userResponse);
    return defer.promise;
}

/**
 * Given an : Object with pet found status and location details for a pet, store in pet collection, found status
 * collection and location collection.
 * @param user_input : Object with pet found status and location details for a pet
 * @returns {*|promise}
 */
function createPet(user_input) {
    var defer = q.defer();
    // handle date input
    user_input.intakeDate = new Date(user_input.intakeDate).toJSON();
    validateUserInput(user_input).then(function (validated_input) {
        if (validated_input) {
            // validate dependencies to ensure transactions
            dependencies(validated_input).then(function () {
                // user input has been validated. store pet details.
                storeInPetModel(validated_input).then(function (from_pet_save) {
                    // stored the pet details, now need to store the found status.
                    storeInFoundStatusModel(from_pet_save).then(function (from_status_save) {
                        // stored the status, now store location
                        storeInLocation(from_status_save).then(function (from_loc_save) {
                            // stored location details, now adjust inventory on the branch
                            adjustInventory(from_loc_save).then(function (from_inv_adj) {
                                filterResponse(from_inv_adj).then(function (final_res) {
                                    defer.resolve(final_res)
                                }).catch(function (err) {
                                    defer.reject(err)
                                })
                            }).catch(function (err) {
                                defer.reject(err)
                            })
                        }).catch(function (err) {
                            defer.reject(err)
                        })
                    }).catch(function (err) {
                        defer.reject(err)
                    })
                }).catch(function (err) {
                    defer.reject(err)
                })
            }).catch(function (err) {
                defer.reject(err)
            });
        } else {
            defer.reject("Validation of user input failed")
        }
    }).catch(function (err) {
        defer.reject(err)
    });
    return defer.promise
}

var search_schema = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "searchCriteria",
    "type": "object",
    "properties": {
        "microchipId": {"type": "string"},
        "name": {"type": "string"},
        "species": {"type": "string", "enum": ["DOG", "CAT", "dog", "cat", "Cat", "Dog"]},
        "gender": {
            "type": "string", "enum": ["MALE", "FEMALE",
                "male", "female", "Male", "Female"]
        },
        "breed": {"type": "string"},
        "category": {
            "type": "string", "enum": ["SMALL", "MEDIUM", "LARGE", "Small", "Medium",
                "Large", "small", "medium", "large"]
        }
    },
    "additionalProperties": false
};


function searchPets(parameters) {
    var defer = q.defer();
    if (Object.keys(parameters).length === 0) {
        getAllPets().then(function (pets) {
            defer.resolve(pets);
        }).catch(function (err) {
            defer.reject(err);
        })
    } else {
        var searchValidation = petValidator.validate(parameters, search_schema);
        if (searchValidation.valid) {
            var search_terms = Object.keys(parameters).map(function (search_key) {
                return parameters[search_key]
            });
            search(search_terms.toString())
                .then(function (pets) {
                    defer.resolve(pets)
                }).catch(function (err) {
                defer.reject(err)
            })
        } else {
            defer.reject("incorrect search criteria")
        }
    }
    return defer.promise
}


function search(searchTerm) {
    return petModel.find({$text: {$search: searchTerm}}).select({
        _id: 0,
        __v: 0
    });
}


module.exports.getAllPets = getAllPets;
module.exports.createPet = createPet;
module.exports.updatePet = updatePet;
module.exports.deletePet = deletePet;
module.exports.findPetByMicrochipID = findPetByMicrochipID;
module.exports.searchPets = searchPets;
module.exports.decreaseInventory = decreaseInventory;

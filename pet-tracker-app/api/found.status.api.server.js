/**
 * Created by shravass on 6/22/17.
 */
module.exports = function (app) {

    app.post('/foundNearBy', isAnyActiveUser, getNearbyFoundPets);
    app.get('/pet/:petId/found_status', isAnyActiveUser, getPetFoundStatusByMicrochipId);
    app.put('/pet/found_status', isAdminOrEmployee, updateFoundStatus);


    // getting service
    var foundStatusService = require('../services/found.status.service.server');
    var userService = require('../services/user.service.server');

    /**
     *  Given a request body with coordinates information,
     *  find found_status information in FoundStatus collection within 5 mile radius
     *  or return 400 error message that the found_status could not be retrieved
     * @param req Request Object
     * @param res Response Object to be returned
     */
    function getNearbyFoundPets(req, res) {
        var coordinates = req.body;
        if (coordinates) {
            foundStatusService.getNearbyFoundPets(coordinates).then(function (foundStatusList) {
                res.statusMessage = "Find successful";
                res.status(200).send(foundStatusList);
            }, function (err) {
                res.status(400).send(err);
            });
        }
        else {
            res.status(417).send(' Co ordinates is empty ');
        }
    }

    /**
     *  Given a request with pet id,
     *  find found_status information in FoundStatus collection for given pet id
     *  or return 400 error message that the found_status could not be retrieved
     * @param req Request Object
     * @param res Response Object to be returned
     */
    function getPetFoundStatusByMicrochipId(req, res) {
        var petMicrochipId = req.params.petId;
        foundStatusService.getPetFoundStatusByMicrochipId(petMicrochipId).then(function (foundStatus) {
            res.statusMessage = "Find successful";
            res.status(200).send(foundStatus);
        }, function (err) {
            res.status(400).send(err);
        });
    }

    /**
     *  Given a request body with update found_status information,
     *  update found_status information in FoundStatus collection if successful
     *  or return 400 error message that the found_status could not be updated
     * @param req Request Object with update found_status information
     * @param res Response Object to be returned
     */
    function updateFoundStatus(req, res) {
        var found_status = req.body;
        if (found_status) {
            foundStatusService.updateFoundStatus(found_status).then(function () {
                res.status(200).send("Record updated successfully");
            }, function (err) {
                res.status(400).send(err);
            })
        }
        else {
            res.status(417).send(' Found Status is empty ');
        }
    }

    function isAdminOrEmployee(req, res, next) {
        userService.findUserByApiKey(req.query.apiKey).then(function (user) {
            if (user) {
                //make sure user trying to make changes has admin/employee rights
                if (user.role.indexOf('ADMIN') !== -1 || user.role.indexOf('EMPLOYEE') !== -1) {
                    next();
                }
                else {
                    res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
                }
            }
            else {
                res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
            }
        }, function (err) {
            res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
        });
    }

    function isAnyActiveUser(req, res, next) {
        userService.findUserByApiKey(req.query.apiKey).then(function (user) {
            if (user) {
                next();
            }
            else {
                res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
            }
        }, function (err) {
            res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
        });
    }
}
;
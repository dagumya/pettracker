/**
 * Created by shravass on 7/6/17.
 */
var petBranchSchema = require('./pet_branch.schema.server');

var mongoose = require('mongoose');

var petBranchModel = mongoose.model('PetBranchModel', petBranchSchema);

petBranchModel.createPetBranch = createPetBranch;
petBranchModel.findPetBranchByBranchUniqueId = findPetBranchByBranchUniqueId;
petBranchModel.findPetBranchByAvailability = findPetBranchByAvailability;
petBranchModel.updatePetBranch = updatePetBranch;
petBranchModel.deletePetBranch = deletePetBranch;
petBranchModel.findAllPetBranches = findAllPetBranches;
petBranchModel.findPetBranchByBranchUniqueIdWithId = findPetBranchByBranchUniqueIdWithId;
petBranchModel.checkAvailability = checkAvailability;
petBranchModel.reactivatePetBranch = reactivatePetBranch;
petBranchModel.checkAllocated = checkAllocated;
petBranchModel.hardDeletePetBranch = hardDeletePetBranch;

module.exports = petBranchModel;


function createPetBranch(petBranch) {
    return petBranchModel.create(petBranch);
}

function findPetBranchByBranchUniqueId(petBranchId) {
    return petBranchModel.findOne({branchUniqueId: petBranchId}).select({
        _id: 0,
        "inventory._id": 0,
        __v: 0
    });
}

function findPetBranchByBranchUniqueIdWithId(petBranchId) {
    return petBranchModel.findOne({branchUniqueId: petBranchId});
}

function checkAvailability(petBranchId, category) {
    return petBranchModel.findOne({
        branchUniqueId: petBranchId, active: true,
        inventory: {$elemMatch: {category: category, availability: {$gt: 0}}}
    });
}

function checkAllocated(petBranchId, category) {
    return petBranchModel.findOne({
        branchUniqueId: petBranchId,
        inventory: {$elemMatch: {category: category, allocated: {$lt: 1}}}
    });
}

function findPetBranchByAvailability(category) {
    return petBranchModel.find({
        active: true,
        inventory: {$elemMatch: {category: category, availability: {$gt: 0}}}
    }).select({
        _id: 0,
        "inventory._id": 0,
        __v: 0
    });
}


function updatePetBranch(updatePetBranch) {
    return petBranchModel.findOneAndUpdate({branchUniqueId: updatePetBranch.branchUniqueId}, {$set: updatePetBranch}, {new: true});
}

/**
 * Soft-delete: pet branch goes inactive instead of being removed from database
 * @param petBranchId
 * @returns petBranch model object
 */
function deletePetBranch(petBranchId) {
    return petBranchModel.findOneAndUpdate({
        branchUniqueId: petBranchId
    }, {
        $set: {"active": false}
    }, {new: true}).select({
        _id: 0,
        "inventory._id": 0,
        __v: 0
    });
}

function reactivatePetBranch(petBranchId) {
    return petBranchModel.findOneAndUpdate({
        branchUniqueId: petBranchId
    }, {
        $set: {"active": true}
    }, {new: true}).select({
        _id: 0,
        "inventory._id": 0,
        __v: 0
    });
}

function findAllPetBranches() {
    return petBranchModel.find().select({
        _id: 0,
        "inventory._id": 0,
        __v: 0
    });
}

function hardDeletePetBranch(branchId) {
    return petBranchModel.findOneAndRemove({branchUniqueId: branchId}).select({
        _id: 0,
        "inventory._id": 0,
        __v: 0
    });
}
/**
 * Created by david on 10/11/17.
 */
/**
 *  Schema for Pet location Model
 *  Model Dependencies: petId, branchId, startDate, endDate, status,
 */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

var locationSchema = new Schema({
    Pet: {
        type: ObjectId,
        ref: 'PetModel',
        required: true
    },
    Branch: {
        type: ObjectId,
        ref: 'PetBranchModel',
        required: true
    },
    startDate: {
        type: Date,
        default: Date.now(),
        required: true
    },
    endDate:{
        type: Date
    },
    status: {
        type: String,
        enum: ['AVAILABLE', 'ADOPTED', 'UNAVAILABLE', 'DECEASED'],
        required: true
    },
    Owner: {
        type: ObjectId,
        ref: 'UserModel'
    }
}, {collection: 'Location'});

module.exports = locationSchema;


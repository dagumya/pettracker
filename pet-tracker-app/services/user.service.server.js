/**
 * Created by shravass on 6/22/17.
 */
var Validator = require('jsonschema').Validator;
var user_validator = new Validator();
var q = require('q');
var user_schema = {
    "title": "User Location",
    "description": "User details for registered users",
    "type": "object",
    "properties": {
        "firstName": {
            "type": "string"
        },
        "lastName": {
            "type": "string"
        },
        "streetAddress": {
            "type": "string"
        },
        "city": {
            "type": "string"
        },
        "state": {
            "type": "string"
        },
        "zipcode": {
            "type": "number"
        },
        "phone": {
            "type": "string"
        },
        "username": {
            "type": "string"
        },
        "password": {
            "type": "string"
        },
        "email": {
            "type": "string"
        }
    },
    "additionalProperties": false,
    "required": ["firstName", "lastName",
        "streetAddress", "city", "state",
        "zipcode", "phone", "username",
        "password", "email"]
};

var userModel = require('../model/user/user.model.server');


/**
 * Auxiliary functions
 */

/**
 * This function is used to find the user by their username. If the user is no longer active in the system, then the
 * user will not be returned
 * @param username
 */
function findUserByUserName(username) {
    var defer = q.defer();
    userModel.findUserByUserName(username).then(function (user) {
        if (user) {
            if (user.active) {
                defer.resolve(user)
            }
            else {
                defer.reject("User is no longer active in our system")
            }
        }
        else {
            defer.reject("Username is not saved in the system")
        }
    }).catch(function (err) {
        console.log(err);
        defer.reject(err)
    });
    return defer.promise
}

/**
 *  Given a request body with new user information, Store user information in User collection if successful
 *  or return 400 error message that the user could not be created
 * @param req Request Object with new user information
 * @param res Response Object to be returned
 */
function createUser(user) {
    var defer = q.defer();
    var schemaValidation = user_validator.validate(user, user_schema);
    if (schemaValidation.valid) {
        userModel.assignApiKey().then(function (key) {
            if (key) {
                if (user) {
                    user.apiKey = key;
                    // validate input

                    userModel.findUserByUserName(user.username).then(function (userFound) {
                        if (userFound) {
                            defer.reject("User already exists in the system");
                        }
                        else {
                            // create user
                            userModel.createUser(user)
                                .then(function (user) {
                                    if (user) {
                                        defer.resolve(user); // status code for successful creating of an object
                                    }
                                    else {
                                        defer.reject(' User could not be created properly ');
                                    }
                                }, function (err) {
                                    defer.reject(' User could not be created: ' + err);
                                });
                        }
                    }, function (err) {
                        defer.reject(' User could not be created: ' + err);
                    })
                }
            }
            else {
                defer.reject("API Key couldn't be generated; user not created");
            }
        });
    } else {
        console.log(schemaValidation.errors);
        defer.reject({
            message: "Please enter valid information to create a user",
            error: schemaValidation.errors
        })
    }
    return defer.promise;
}

function getApiKey(username, password) {
    var defer = q.defer();
    userModel.findUserByCredentials(username, password).then(function (user) {
        if (user) {
            defer.resolve(user.apiKey);
        }
        else {
            defer.reject("User is not registered in the system");
        }
    }, function (err) {
        defer.reject(err)
    });
    return defer.promise
}


//Function for updating user role
function updateUserRole(operation, username, role) {
    var defer = q.defer();
    //check to make sure that the role to assign is valid
    if (!(role === 'USER' || role === 'EMPLOYEE' || role === 'ADMIN')) {
        defer.reject("Invalid role type");
    } else if(!(operation === 'A' || operation === 'R')){
        defer.reject("Invalid Operation type type");
    }
    else {
        //if user not active, will not be returned by this function
        findUserByUserName(username).then(function (user) {
            //check to make sure username is valid
            if (user) {
                //update user role
                if(operation === 'A'){
                    userModel.assignUserRole(user, role).then(function (user) {
                        if (user) {
                            defer.resolve(user);
                        }
                        else {
                            defer.reject("unable to update user role")
                        }
                    }, function (err) {
                        defer.reject("User details could not be found");
                    })
                } else {
                    userModel.removeUserRole(user, role).then(function (user) {
                        if (user) {
                            defer.resolve(user);
                        }
                        else {
                            defer.reject("unable to update user role")
                        }
                    }, function (err) {
                        defer.reject("User details could not be found");
                    })
                }
            }
            else {
                defer.reject("Given username is not found in the system");
            }
        }, function (err) {
            defer.reject(err)
        });
    }
    return defer.promise;
}

var user_update_schema = {
    "title": "User Location",
    "description": "User details for registered users",
    "type": "object",
    "properties": {
        "firstName": {
            "type": "string"
        },
        "lastName": {
            "type": "string"
        },
        "streetAddress": {
            "type": "string"
        },
        "city": {
            "type": "string"
        },
        "state": {
            "type": "string"
        },
        "zipcode": {
            "type": "number"
        },
        "phone": {
            "type": "string"
        },
        "username": {
            "type": "string"
        },
        "password": {
            "type": "string"
        },
        "email": {
            "type": "string"
        }
    },
    "additionalProperties": false
};

function updateUser(username, userToBeUpdated) {
    var defer = q.defer();
    var schemaValidation = user_validator.validate(userToBeUpdated, user_update_schema);
    if (schemaValidation.valid) {
        findUserByUserName(username).then(function (user) {
            if (user) {
                userModel.updateUserByUsername(username, userToBeUpdated)
                    .then(function (user) {
                        if (user) {
                            defer.resolve(user);
                        }
                        else {
                            defer.reject("User could not be updated")
                        }
                    }, function (err) {
                        defer.reject(err)
                    })
            } else {
                defer.reject("User is not registered in the system")
            }
        }).catch(function (err) {
            defer.reject(err)
        });
    } else {
        console.log(schemaValidation.errors);
        defer.reject({
            message: "Please enter valid information to create a user",
            error: schemaValidation.errors
        })
    }
    return defer.promise;
}

/**
 * Given a DELETE request with userId in the parameter of the request object. Use that user Id to delete the user from our database
 * @param username of the user to be deleted from the system
 */
function deleteUser(username) {
    var defer = q.defer();
    findUserByUserName(username).then(function (user) {
            userModel.deleteUser(username)
                .then(function (user) {
                    if (user) {
                        defer.resolve(user)
                    }
                    else {
                        defer.reject("User not found, Make sure user is not deactivated and is registered in the system")
                    }
                }, function (err) {
                    defer.reject(err)
                });

    }, function (err) {
        defer.reject(err);
    });

    return defer.promise;

}

/**
 * Function to reactivate user that has been deleted
 * @param username
 */
function reactivateUser(username) {
    var defer = q.defer();
    userModel.reactivateUser(username).then(function (user) {
        if (user) {
            defer.resolve(user)
        }
        else {
            defer.reject("Could not reactivate user. Check that correct ID was used")
        }
    }, function(err) {
        defer.reject("User could not be reactivated: " + err);
    });

    return defer.promise;
}

function hardDelete(userId) {
    var defer = q.defer();
    userModel.hardDeleteUser(userId).then(function (user) {
        if(user){
            defer.resolve({deleted_user : user})
        } else{
            defer.reject('User could not be deleted');
        }
    }).catch(function (err) {
        defer.reject(err)
    });
    return defer.promise;
}

function hardCreate(user) {
    var defer = q.defer();
    userModel.createUser(user).then(function (user) {
        if(user){
            defer.resolve(user)
        } else{
            defer.reject('User could not be created');
        }
    }).catch(function (err) {
        defer.reject(err)
    });
    return defer.promise;
}

function findUserByApiKey(apikey) {
    var defer = q.defer();
    userModel.findUserByApiKey(apikey).then(function (user) {
        if (user) {
            if (user.active) {
                defer.resolve(user)
            }
            else {
                defer.reject("User is no longer active in our system")
            }
        }
        else {
            defer.reject("Not a valid api key")
        }
    }).catch(function (err) {
        defer.reject(err);
    });
    return defer.promise;
}


module.exports.createUser = createUser;
module.exports.findUserByUserName = findUserByUserName;
module.exports.getApiKey = getApiKey;
module.exports.updateUser = updateUser;
module.exports.deleteUser = deleteUser;
module.exports.updateUserRole = updateUserRole;
module.exports.hardDelete = hardDelete;
module.exports.reactivateUser = reactivateUser;
module.exports.findUserByApiKey = findUserByApiKey;
module.exports.hardCreate = hardCreate;


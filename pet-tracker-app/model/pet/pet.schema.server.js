/*
* Created by Haffner on 10/15/2017 following example of pet_branch models
*/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var petSchema = mongoose.Schema({
    microchipId: {type: String, unique: true},
    name: String,
    species: {type: String, enum: ['DOG', 'CAT']},
    gender: {type: String, enum: ['MALE', 'FEMALE']},
    breed: String,
    category: {type: String, enum: ['SMALL', 'MEDIUM', 'LARGE']},
    weight: Number, //not searchable - searched by weight category
    age: Number,
    colorDescription: String, //not searchable
    personality: String, //not searchable
    notes: String, //not searchable
    intakeDate: Date,
    neutered: Boolean,
    disabled: Boolean,
    imageUrl: String
}, {collection: "Pet"});

petSchema.index({name: 'text', breed: 'text', species : 'text' , category : 'text'});

module.exports = petSchema;
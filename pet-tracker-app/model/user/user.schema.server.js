/**
 * Created by shravass on 7/6/17.
 */

var mongoose = require('mongoose');

var userSchema = mongoose.Schema({

    username: {type: String, unique: true, required: true},
    password: {type: String},
    apiKey: {type: String, unique: true},
    firstName: String,
    lastName: String,
    email: String,
    phone: String,
    dateCreated: {type: Date, default: new Date()},
    role: { type: [ {type: String, enum: ['USER', 'EMPLOYEE', 'ADMIN']}], default: ["USER"]},
    streetAddress: String,
    city: String,
    state: String,
    zipcode: Number,
    active: {type: Boolean, default: true}
}, { collection : "User"});


module.exports = userSchema;

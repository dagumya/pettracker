/**
 * Created by shravass on 6/22/17.
 */
module.exports = function (app) {


    app.get('/', homePage);

    function homePage(req, res) {
        var data = "<!doctype HTML> <html> <head> <title>Seattle Pet Tracker</title> </head> <body> <h3>Welcome to Seattle Pet Tracker </h3> <h4> How to use the application? <a href=\"https://app.swaggerhub.com/apis/shravass/seattle-pet_tracker/1.0.2\" target=\"_blank\">Swagger Documentation</a> </h4> </body> </html>";
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(data);
        res.end();
    }


};
/**
 * Created by shravass on 7/6/17. updated by Haffner 10/26/17
 */
var userSchema = require('./user.schema.server');
var mongoose = require('mongoose');
var q = require('q');
var userModel = mongoose.model('UserModel', userSchema);




function createUser(user) {
    return userModel.create(user);
}

function findUserByUserName(username) {
    return userModel.findOne({username: username});
}

function findUserByApiKey(apiKey) {
    return userModel.findOne({apiKey: apiKey});
}


function checkForKey(apiKey) {
    var defer = q.defer();
    findUserByApiKey(apiKey).then(function (user) {
        if (user) {
            defer.resolve(false)
        }
        else {
            defer.resolve(true)
        }
    });
    return defer.promise;
}

function loop(apiKey) {
    var hat = require('hat');
    var defer = q.defer();
    checkForKey(apiKey).then(function (available) {
        if (available) {
            defer.resolve(apiKey) //GETS HUNG UP HERE AFTER GENERATING NEW API KEY BECAUSE ORIGINAL WASN'T UNIQUE
        }
        else {
            defer.resolve(loop(hat()));
        }
    });
    return defer.promise;
}

function validateApiKey(apiKey) {
    var defer = q.defer();
    loop(apiKey).then(function (key) {
        if (key) {
            defer.resolve(key);
        }
        else {
            defer.reject("something went wrong");
        }
    });
    return defer.promise;
}

function assignApiKey() {
    var defer = q.defer();
    var hat = require('hat');
    var apiKey = hat();
    //var apiKey = "1234"; //FOR TESTING WITH API KEY THAT ALREADY EXISTS
    validateApiKey(apiKey).then(function (key) {
        defer.resolve(key);
    });
    return defer.promise;
}

function findUserByCredentials(username, password) {
    return userModel.findOne({username: username, password: password});
}

function updateUserByUsername(username, updateUser){
    return userModel.findOneAndUpdate({
        username: username
    }, {
        $set: updateUser
    }, {new : true});
}

function assignUserRole(user, role) {
    return userModel.findOneAndUpdate({
        username: user.username
    }, {
        $push: {role: role}
    }, {new : true})
}

function removeUserRole(user, role) {
    return userModel.findOneAndUpdate({
        username: user.username
    }, {
        $pull: {role: role}
    }, {new : true})
}

/**
 * Soft-delete: user goes inactive instead of being removed from database
 * @param username
 * @returns user model object
 */
function deleteUser(username) {
    return userModel.findOneAndUpdate({
        username: username
    }, {
        $set: {"active" : false}
    }, {new : true})
}

/**
 * Reactivates user after being deleted
 * @param username
 * @returns user model
 */
function reactivateUser(username) {
    return userModel.findOneAndUpdate({
        username: username
    }, {
        $set: {"active" : true}
    }, {new : true})
}

function hardDeleteUser(username) {
    return userModel.findOneAndRemove({
        username: username
    })
}

userModel.createUser = createUser;
userModel.findUserByUserName = findUserByUserName;
userModel.findUserByApiKey = findUserByApiKey;
userModel.assignApiKey = assignApiKey;
userModel.deleteUser = deleteUser;
userModel.updateUserByUsername = updateUserByUsername;
userModel.findUserByCredentials = findUserByCredentials;
userModel.assignUserRole = assignUserRole;
userModel.removeUserRole = removeUserRole;
userModel.hardDeleteUser = hardDeleteUser;
userModel.reactivateUser = reactivateUser;

module.exports = userModel;
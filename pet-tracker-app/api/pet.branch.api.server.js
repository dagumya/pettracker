/**
 * Created by shravass on 6/22/17.
 */
module.exports = function (app) {


    app.post('/pet_branch', isAdmin, createPetBranch);

    app.get('/pet_branch', isAnyActiveUser, findPetBranchByAvailability);

    app.get('/pet_branch/:branchUniqueId', isAnyActiveUser, findPetBranchById);

    app.put('/pet_branch', isAdminOrEmployee, updatePetBranch);

    app.delete('/pet_branch/:branchUniqueId', isAdmin, deletePetBranch);

    app.get('/pet_branches', isAnyActiveUser, findAllPetBranches);

    app.put('/activateBranch/:branchUniqueId', isAdminOrEmployee, reactivateBranch);


    // getting service
    var petBranchService = require('../services/pet.branch.service.server');
    var userService = require('../services/user.service.server');

    /**
     *  Given a request body with new pet_branch information, Store pet_branch information in PetBranch collection if successful
     *  or return 405 error message that the pet_branch could not be created
     * @param req Request Object with new pet_branch information
     * @param res Response Object to be returned
     */
    function createPetBranch(req, res) {
        var pet_branch = req.body;
        if (pet_branch) {
            petBranchService.createPetBranch(pet_branch)
                .then(function (pet_branch) {
                    res.statusMessage = "created successfully";
                    res.status(200).send(pet_branch);
                }, function (err) {
                    res.status(405).send(err);
                })
        }
        else {
            res.status(417).send(' PetBranch is empty ');
        }
    }

    /***
     *  Given a request object with pet category. Search for pet_branch, based on those availability.
     *  Return pet_branches as a Json
     * @param req Request object with pet_category
     * @param res Request object
     */
    function findPetBranchByAvailability(req, res) {
        if(req.query.category){
            var category = req.query.category.toUpperCase();
            petBranchService.findPetBranchByAvailability(category)
                .then(function (branches) {
                    res.json(branches);
                }, function (err) {
                    res.status(405).send(err);
                });
        } else{
            res.status(405).send("Please specify a category");
        }

    }

    /**
     *  Given a branch unique Id, search for pet_branch document. Return the pet_branch details as a json
     *  if successful or 400 status code if there is some error
     * @param req -> Request object with userId
     * @param res -> Response returned
     */
    function findPetBranchById(req, res) {
        var branchId = req.params.branchUniqueId;
        petBranchService.findPetBranch(branchId)
            .then(function (branch) {
                res.status(200).send(branch);
            }, function (err) {
                res.status(400).send(err);
            });
    }

    /**
     * Given branch details about a pet_branch. Update a pet_branch record in the database. If the pet_branch record
     * update is not successful then return a 405. If it is successful then return updated pet_branch details as a json
     * @param req Request object with branch_details in the body for updating
     * @param res Response object returned
     */
    function updatePetBranch(req, res) {
        var pet_branch = req.body;
        if (pet_branch) {
            petBranchService.updatePetBranch(pet_branch)
                .then(function (branch) {
                    res.statusMessage = "updated successfully";
                    res.status(200).send(branch);
                }, function (err) {
                    res.status(405).send(err);
                })
        }
        else {
            res.status(417).send(' PetBranch is empty ');
        }
    }

    /**
     * Given branch unique id. Delete a pet_branch record in the database. If the pet_branch record
     * delete is not successful then return a 405. If it is successful then return updated pet_branch details as a json
     * @param req Request object with branch_details in the body for updating
     * @param res Response object returned
     */
    function deletePetBranch(req, res) {
        var branchId = req.params.branchUniqueId;
        petBranchService.deletePetBranch(branchId).then(function (branch) {
            if (branch) {
                res.statusMessage = "deleted successfully";
                res.status(200).send(branch);
            }
        }, function (err) {
            res.status(400).send(err);
        });
    }

    /**
     * Allows user to reactivate pet branch with branchUniqueId
     * @param req
     * @param res
     */
    function reactivateBranch(req, res) {
        var branchId = req.params.branchUniqueId;
        petBranchService.reactivatePetBranch(branchId).then(function (branch) {
            if (branch) {
                res.statusMessage = "branch successfully reactivated";
                res.status(200).send(branch)
            }
        }, function (err) {
            res.status(400).send(err);
        }) ;
    }

    /**
     * Gives list of pet_branches in the database.
     * If it is successful then return array of pet_branch details as a json
     * Else 400 status code
     * @param req Request
     * @param res Response object returned
     */
    function findAllPetBranches(req, res) {
        petBranchService.findAllPetBranches().then(function (petBranches) {
            if (petBranches) {
                res.status(200).send(petBranches);
            }
        }, function (err) {
            res.status(400).send(err);
        })

    }

    function isAdminOrEmployee(req, res, next) {
        userService.findUserByApiKey(req.query.apiKey).then(function (user) {
            if (user) {
                //make sure user trying to make changes has admin/employee rights
                if (user.role.indexOf('ADMIN') !== -1 || user.role.indexOf('EMPLOYEE') !== -1) {
                    next();
                }
                else {
                    res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
                }
            }
            else {
                res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
            }
        }, function (err) {
            res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
        });
    }

    function isAnyActiveUser(req, res, next) {
        userService.findUserByApiKey(req.query.apiKey).then(function (user) {
            if (user) {
                next();
            }
            else {
                res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
            }
        }, function (err) {
            res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
        });
    }

    function isAdmin(req, res, next) {
        userService.findUserByApiKey(req.query.apiKey).then(function (user) {
            if (user) {
                //make sure user trying to make changes has admin rights
                if (user.role.indexOf('ADMIN') !== -1) {
                    next();
                }
                else {
                    res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
                }
            }
            else {
                res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
            }
        }, function (err) {
            res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
        });
    }
};
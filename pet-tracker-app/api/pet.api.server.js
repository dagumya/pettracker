module.exports = function (app) {
    /**
     DEPENDENCIES
     */
    var petService = require("../services/pet.service.server.js");
    var userService = require('../services/user.service.server');
    /**
     * ROUTE HANDLING
     */

    app.post('/pet', isAdminOrEmployee, createPet);
    app.get('/pet', isAnyActiveUser, getAllPets);
    app.get('/pet/:microchipId', isAnyActiveUser, getPetById);
    app.put('/pet/:microchipId', isAdminOrEmployee, updatePet);
    app.delete('/pet/:microchipId', isAdminOrEmployee, deletePet);
    app.post("/search", isAnyActiveUser, searchPets);

    /**
     * ROUTE HANDLERS
     */
    /**
     * Given a request object body with pet details about the pet, found status and location, save details to pet
     * collection, found status collection and location collection. If saving to database is successfull return pet
     * details
     * @param req : Request Object
     * @param res : Response Object
     * @returns {Response}
     */
    function createPet(req, res) {
        if (Object.keys(req.body).length !== 0) {
            petService.createPet(req.body)
                .then(function (success) {
                    res.status(201);
                    return res.json(success);
                }).catch(function (err) {
                console.log(err);
                res.status(400);
                return res.send("Unable to create pet: " + err);
            })
        } else {
            res.status(400).send("Response body is empty");
        }
    }

    /**
     *  Retrieve all the pet records in our database
     * @param req : Request Object
     * @param res : Response Object
     * @returns {Response}
     */
    function getAllPets(req, res) {
        petService.getAllPets()
            .then(function (pets) {
                res.status(200);
                return res.json(pets);
            }).catch(function (err) {
            console.log(err.message);
            console.log(err.code);
            res.status(400);
            return res.send("Unable to get All pets: " + err)
        })
    }

    /**
     *  Retrieve a pet record by pet microchipId. Pet will only be retrived if a pet with the referenced microchip given
     *  as part of the request parameters exists in our system otherwise an re
     * @param req : Request Object
     * @param res : Response Object
     * @returns {Response}
     */
    function getPetById(req, res) {
        // handle GET /pet/:microchipId
        if (req.params.microchipId) {
            petService.findPetByMicrochipID(req.params.microchipId)
                .then(function (pet) {
                    if (pet) {
                        delete pet._doc._id;
                        delete pet._doc.__v;
                        res.status(200);
                        return res.json(pet)

                    } else {
                        res.status(400);
                        return res.send("Pet is not registered in our system.")
                    }
                }).catch(function (err) {
                res.status(400);
                return res.send("Unable to retrive pet specified by " + req.params.microchipId + " : " + err);

            })
        } else {
            return res.status(400).send("Please provide a microchip id i.e /pet/id")

        }
    }

    /**
     * Updated a pet, referenced by microchipId provided in the request parameters
     * @param req : Request Object
     * @param res : Response Object
     * @returns {Response}
     */
    function updatePet(req, res) {
        if (req.params.microchipId && Object.keys(req.body).length !== 0) {
            petService.updatePet(req.params.microchipId, req.body)
            // return the updated pet to the user
                .then(function (updated_pet) {
                    res.status(200);
                    return res.json(updated_pet)
                }).catch(function (err) {
                console.log(err.message);
                res.status(400);
                return res.send("Pet was not successfully updated: " + err)
            });
        } else {
            return res.status(400)
                .send("Please provide a microchipId for the pet " +
                    "as well as new update details ")
        }
    }

    /**
     * Delete Pet referenced by microchipId provided in the request parameters
     * @param req : Request Object
     * @param res : Response Object
     * @returns {Response}
     */
    function deletePet(req, res) {
        // handle DELETE /pet/:microchipId
        if (req.params.microchipId) {
            petService.deletePet(req.params.microchipId)
                .then(function (deleted_pet) {
                    if (deleted_pet) {
                        res.status(200);
                        return res.json(deleted_pet)
                    } else {
                        res.status(400);
                        return res.send("Unable to delete pet")
                    }

                }).catch(function (err) {
                res.status(400);
                return res.send(err);
            })
        } else {
            return res.status(400)
                .send("Please provide a microchipId")
        }

    }

    function searchPets(req, res){
        var parameters = req.body;
        // if the user provides a microchip id in search parameters
        if (parameters.microchipId){
            petService.findPetByMicrochipID(parameters.microchipId)
                .then(function(pet){
                    return res.json(pet)
                }).catch(function (err) {
                return res.status(400).send("MicrochipId not registered in system")
            })
        }else{
            // if user provides other parameters other than microchipId
            petService.searchPets(parameters).then(function(result){
                if (result){
                    res.status(200);
                    return res.send(result)
                }else{
                    res.status(400);
                    return res.send("Unable to find pets based on search parameters")

                }
            }).catch(function(err){
                res.status(400);
                return res.send(err)
            })
        }

    }

    function isAdminOrEmployee(req, res, next) {
        userService.findUserByApiKey(req.query.apiKey).then(function (user) {
            if (user) {
                //make sure user trying to make changes has admin/employee rights
                if (user.role.indexOf('ADMIN') !== -1 || user.role.indexOf('EMPLOYEE') !== -1) {
                    next();
                }
                else {
                    res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
                }
            }
            else {
                res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
            }
        }, function (err) {
            res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
        });
    }

    function isAnyActiveUser(req, res, next) {
        userService.findUserByApiKey(req.query.apiKey).then(function (user) {
            if (user) {
                next();
            }
            else {
                res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
            }
        }, function (err) {
            res.status(401).send('You do not have permission to perform this operation. Not a valid USER');
        });
    }


};

/*
* Updated by Haffner in 10/15/2017 following example of pet_branch models
*/

var petSchema = require('./pet.schema.server');

var mongoose = require('mongoose');
var q = require('q');

var petModel = mongoose.model('PetModel', petSchema);

//Pet create, update, delete
petModel.createPet = createPet;
petModel.findPetByID = findPetByID; //by microchipId
petModel.findPetsByName = findPetsByName;
petModel.findPetsByGender = findPetsByGender;
petModel.findPetsBySpecies = findPetsBySpecies;
petModel.findPetsByBreed = findPetsByBreed;
petModel.findPetsByCategory = findPetsByCategory;
petModel.findPetsByAge = findPetsByAge;
petModel.findPetsByIntakeDate = findPetsByIntakeDate;
petModel.findPetsByNeuteredStatus = findPetsByNeuteredStatus;
petModel.findPetsByDisabledStatus = findPetsByDisabledStatus; 
petModel.updatePet = updatePet;
petModel.deletePet = deletePet; //how are we handling deleting the pet?
petModel.findAllPets = findAllPets;

// exports it for use in other files
module.exports = petModel;

function createPet(pet) {
    return petModel.create(pet);
}

function findPetByID(petID) {
    return petModel.findOne({microchipId: petID}); //unique identifier
}

function findPetsByName(petName) {
    return petModel.find({name: petName});
}

function findPetsByGender(gender) {
    return petModel.find({gender: gender});
}

function findPetsBySpecies(petSpecies) {
    return petModel.find({species: petSpecies});
}

function findPetsByBreed(petBreed) {
    return petModel.find({breed: petBreed});
}

function findPetsByCategory(petCategory) {
    return petModel.find({category: petCategory});
}

function findPetsByAge(petAge) {
    return petModel.find({age: petAge});
}

function findPetsByIntakeDate(PetIntakeDate) {
    return petModel.find({intakeDate: PetIntakeDate});
}

function findPetsByNeuteredStatus(neuteredStatus) {
    return petModel.find({neutered: neuteredStatus});
}

function findPetsByDisabledStatus(disabilityStatus) {
    return petModel.find({disabled: disabilityStatus});
}

function updatePet(id, updatePet) {
    return petModel.findOneAndUpdate({microchipId: id}, {$set: updatePet} , {new : true});
}

function deletePet(id) {
    return petModel.findOneAndRemove({microchipId: id});
}

function findAllPets() {
    return petModel.find().select({
        _id: 0,
        __v: 0
    });
}
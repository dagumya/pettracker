/**
 * Created by shravass on 7/6/17.
 */
var mongoose = require('mongoose');

var pointSchema = mongoose.Schema({
    type: {type: String, required: true, default: "Point", enum: ["Point"]},
    coordinates: [{type: Number}]
});


var foundStatusSchema = mongoose.Schema({

    Pet: {type: mongoose.Schema.Types.ObjectId, ref: 'PetModel', required: true, unique: true},
    location: {type: pointSchema, required: true, index: '2dsphere'},
    foundDate: {type: Date, default: new Date(), required: true},
    FoundBy: {type: mongoose.Schema.Types.ObjectId, ref: 'UserModel', required: true}

}, {collection: 'FoundStatus'});

foundStatusSchema.index({location:"2dsphere"});

module.exports = foundStatusSchema;

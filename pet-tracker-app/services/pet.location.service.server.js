/**
 * Created by david on 9/30/17.
 */
"use strict";

/**
 * DEPENDENCIES
 */

    //initializing q
var q = require('q');

// json schema validation instance
var Validator = require('jsonschema').Validator;
var pet_location_validator = new Validator();


// Pet Location schema defined for validation
var pet_location_schema = {
    "title": "Pet Location",
    "description": " Pet location details",
    "type": "object",
    "properties": {
        "microchipId": {
            "type": "string"
        },
        "branchId": {
            "type": "string"
        },
        "status": {
            "description": "Current status of the pet at the current location",
            "type": "string",
            "enum": ["ADOPTED", "AVAILABLE", "UNAVAILABLE", "DECEASED"]
        },
        "owner_username": {
            "description": "owner username for a user already registered in the system",
            "type": "string"
        }
    },
    "additionalProperties": false,
    "required": ["microchipId", "branchId", "status"]

};

// getting model
var petLocationModel = require('../model/location/location.model.server');

//getting other services
var petService = require('./pet.service.server');
var userService = require('./user.service.server');
var petBranchService = require('./pet.branch.service.server');

// defining service methods
module.exports.create_location = create_location;
module.exports.get_PetLocations = get_PetLocations;
module.exports.getPetCurrentLocation = getPetCurrentLocation;
module.exports.get_PetsInBranch = get_PetsInBranch;
module.exports.getCurrentPetsInBranch = getCurrentPetsInBranch;
module.exports.update_location = update_location;
module.exports.deleteLocationsByPet = deleteLocationsByPet;
module.exports.updatePetStatus = updatePetStatus;

// status enum
var petStatusEnum = ["ADOPTED", "AVAILABLE", "UNAVAILABLE", "DECEASED"];

/**
 * HELPER FUNCTIONS
 */
function filterLocationResponse(location) {
    var location_obj = {
        Pet: location.Pet,
        Branch: location.Branch,
        Owner: location.Owner,
        Start_date: location.startDate
    };
    if (location.hasOwnProperty('endDate')) {
        location_obj = location.endDate
    }
    return location_obj;
}

function create_location(pet_location) {
    var defer = q.defer();
    var schemaValidation = pet_location_validator.validate(pet_location, pet_location_schema);
    if (schemaValidation.valid) {

        // checking status with owner
        if(pet_location.status === 'AVAILABLE' || pet_location.status === 'UNAVAILABLE' || pet_location.status === 'DECEASED' ){
            if(pet_location.owner_username){
                defer.reject(' Invalid combination of status and owner ');
            }
        }

        var pet_loc = {
            status: pet_location.status
        };

        // use microchip id to get pet _id
        petService.findPetByMicrochipID(pet_location.microchipId)
            .then(function (pet) {
                pet_loc.Pet = pet._id;
                return petBranchService.findPetBranchByBranchUniqueId(pet_location.branchId);
            })
            .then(function (branch) {
                pet_loc.Branch = branch._id;
                if (pet_location.owner_username) {
                    return userService.findUserByUserName(pet_location.owner_username);
                }
                else {
                    return Promise.resolve();
                }
            })
            .then(function (userId) {
                if (userId) {
                    pet_loc.Owner = userId;
                }
                petLocationModel.createPetLocation(pet_loc)
                    .then(function (petLocation) {
                        if (petLocation) {
                            defer.resolve(filterLocationResponse(petLocation));
                        } else {
                            defer.reject('Location details could not be created');
                        }
                    }, function (err) {
                        defer.reject(err);
                    })
            })
            .catch(function (err) {
                defer.reject(err);
            })

    } else {
        defer.reject("Please provide the correct location details: " + schemaValidation.errors);
    }
    return defer.promise;
}

function get_PetLocations(microchipId) {
    var defer = q.defer();
    petService.findPetByMicrochipID(microchipId)
        .then(function (pet) {
            return petLocationModel.findPetLocations(pet);
        })
        .then(function (locations) {
            defer.resolve(locations);
        })
        .catch(function (err) {
            defer.reject(err);
        });

    return defer.promise;
}

function getPetCurrentLocation(microchipId) {
    var defer = q.defer();
    petService.findPetByMicrochipID(microchipId)
        .then(function (pet) {
            return petLocationModel.findCurrentLocationOfPet(pet);
        })
        .then(function (location) {
            defer.resolve(location);
        })
        .catch(function (err) {
            defer.reject(err);
        });

    return defer.promise;
}



function get_PetsInBranch(branchId) {
    var defer = q.defer();
    petBranchService.findPetBranchByBranchUniqueId(branchId)
        .then(function (branch) {
            return petLocationModel.findPetsHistoryInBranch(branch);
        })
        .then(function (locations) {
            defer.resolve(locations);
        })
        .catch(function (err) {
            defer.reject(err);
        });

    return defer.promise;
}

function getCurrentPetsInBranch(branchId) {
    var defer = q.defer();
    petBranchService.findPetBranchByBranchUniqueId(branchId)
        .then(function (branch) {
            return petLocationModel.findCurrentPetsInBranch(branch);
        })
        .then(function (locations) {
            defer.resolve(locations);
        })
        .catch(function (err) {
            defer.reject(err);
        });

    return defer.promise;
}

/**
 *  Given a request object with the location updates. Update the location specified by the location id
 *  @param petId -> microchipId of pet to transfer
 *  @param branchId -> branch to transfer pet to
 */
function update_location(petId, branchIdToBeTransferred) {
    var defer = q.defer();
    petService.findPetByMicrochipID(petId).then(function(pet) {
        petBranchService.checkBranchInventory(branchIdToBeTransferred, pet.category).then(function(petBranch) {
            if (petBranch) {
                findCurrentPetLocation(pet).then(function(loc) {
                    if(loc.Branch.branchUniqueId === branchIdToBeTransferred){
                        defer.reject("Pet is already present in the same branch");
                    } else if (loc.status === 'ADOPTED' || loc.status === 'DECEASED'){
                        defer.reject("Current Pet Status doesn't allow it to be transferred");
                    }
                    else{
                        //update old branch to have 1 less animal of that category
                        var new_parameters = {
                            branchUniqueId: loc.Branch.branchUniqueId,
                            category : pet.category,
                            noOfPetsAllocated: -1
                        };
                        petBranchService.updateInventoryDetails(new_parameters).then(function(branch){
                            //update pet's old location with a new end date
                            petLocationModel.updateLocEndDate(pet, Date.now()).then(function(loc) {
                                //update new branch to have 1 more animal of that category
                                var new_parameters = {
                                    branchUniqueId: branchIdToBeTransferred,
                                    category : pet.category,
                                    noOfPetsAllocated: 1
                                };
                                petBranchService.updateInventoryDetails(new_parameters).then(function(branch){
                                    //create new location for pet
                                    var loc_rec = {
                                        microchipId: pet.microchipId,
                                        status: loc.status,
                                        branchId: branch.branchUniqueId
                                    };
                                    create_location(loc_rec).then(function(loc){
                                        defer.resolve(loc);
                                    }, function(err) {
                                        defer.reject(err);
                                    });
                                }, function(err) {
                                    defer.reject(err);
                                });
                            }, function(err){
                                defer.reject(err)
                            })
                        }, function(err) {
                            defer.reject(err);
                        });
                    }
                }, function(err) {
                    defer.reject(err);
                });
            }
            else {
                defer.reject("Unable to transfer pet to this location");
            }
        }, function(err) {
            defer.reject(err);
        });
    }).catch(function (err) {
        defer.reject(err);
    });

    return defer.promise;
}

/**
 *
 * @param pet
 */
function findCurrentPetLocation(pet) {
    var defer = q.defer();
    petLocationModel.findCurrentLocationOfPet(pet).then(function(loc) {
        if (loc) {
            defer.resolve(loc);
        }
        else {
            defer.reject("Cannot find location of pet")
        }

    }).catch(function (err) {
        defer.reject(err);
    });

    return defer.promise;
}

function deleteLocationsByPet(microchipId) {
    var defer = q.defer();
    petService.findPetByMicrochipID(microchipId)
        .then(function (pet) {
            return petLocationModel.deleteLocationsByPet(pet);
        })
        .then(function (locations) {
            defer.resolve(locations);
        })
        .catch(function (err) {
            defer.reject(err);
        });

    return defer.promise;

}

function updatePetStatus(microchipId, petStatus, ownerUserName, branchUniqueId) {
    var defer = q.defer();
    //check if pet status is valid
    if (petStatusEnum.indexOf(petStatus) !== -1) {
        //if adopted need owner name
        if (petStatus === 'ADOPTED') {
            if (!ownerUserName) {
                defer.reject(' Owner is mandatory for this status ');
                return defer.promise;
            }
        } else {
            if (ownerUserName) {
                defer.reject(' Invalid combination of status and owner ');
                return defer.promise;
            }
        }

        var loc_rec = {};
        loc_rec.status = petStatus;

        petService.findPetByMicrochipID(microchipId).then(function (pet) {
            loc_rec.Pet = pet;
            //if username present find user
            if (ownerUserName) {
                return userService.findUserByUserName(ownerUserName);
            }
            else {
                return Promise.resolve();
            }
        }).then(function (user) {
            if (user) {
                loc_rec.Owner = user;
            }
            return getPetCurrentLocation(microchipId);
        }).then(function (loc) {
            if (loc) {
                // since current location is available it would have been in available / unavailable state before
                loc_rec.Branch = loc.Branch.branchUniqueId;
                if (petStatus === 'ADOPTED' || petStatus === 'DECEASED') {
                    var new_parameters = {
                        branchUniqueId: loc.Branch.branchUniqueId,
                        category: loc_rec.Pet.category,
                        noOfPetsAllocated: -1
                    };
                    return petBranchService.updateInventoryDetails(new_parameters).then(function (branch) {
                        return petLocationModel.updateLocEndDate(loc_rec.Pet, Date.now());
                    }).catch(function (err) {
                        defer.reject(err);
                        return defer.promise;
                    });
                } else {
                    return petLocationModel.updateLocEndDate(loc_rec.Pet, Date.now());
                }
            } else {
                // since current location is not available it would have been in adopted / deceased state before
                if (petStatus === 'ADOPTED' || petStatus === 'DECEASED') {
                    return Promise.reject("The pet is not tracked in any branch. Cannot update the status");
                } else {
                    if (branchUniqueId) {
                        loc_rec.Branch = branchUniqueId;
                        return petBranchService.checkBranchInventory(branchUniqueId, loc_rec.Pet.category).then(function () {
                            var new_parameters = {
                                branchUniqueId: branchUniqueId,
                                category: loc_rec.Pet.category,
                                noOfPetsAllocated: 1
                            };
                            return petBranchService.updateInventoryDetails(new_parameters)
                        }).catch(function (err) {
                            defer.reject(err);
                            return defer.promise;
                        });
                    } else {
                        return Promise.reject("Need a branch in which pet will be put");
                    }
                }
            }
        }).then(function () {
            if (petStatus === 'AVAILABLE' || petStatus === 'UNAVAILABLE') {
                var locationFinal = {
                    microchipId: loc_rec.Pet.microchipId,
                    status: loc_rec.status,
                    branchId: loc_rec.Branch
                };
                return create_location(locationFinal);
            } else {
                // putting entry for adopted/deceased with same start date and end date
                var locationFinal = {
                    microchipId: loc_rec.Pet.microchipId,
                    status: loc_rec.status,
                    branchId: loc_rec.Branch
                };
                return create_location(locationFinal).then(function (loc) {
                    return petLocationModel.updateLocEndDateAndOwner(loc_rec.Pet, Date.now(), loc_rec.Owner);
                }).catch(function (err) {
                    defer.reject(err);
                });
            }

        }).then(function (loc) {
            defer.resolve(loc);
        }).catch(function (err) {
            defer.reject(err);
        });
    } else {
        defer.reject("Invalid Pet status");
    }
    return defer.promise;
}


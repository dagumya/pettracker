//using express with node js
var express = require('express');
var path = require('path'); // public contents
var bodyParser = require('body-parser'); //body parser
var cookieParser = require('cookie-parser'); //cookie parser

//initialize app as an express application
var app = express();

//process.env.port - if we are deploying in heroku we are assigned a port number
var PORT = process.env.PORT || 4000;

/**
 Middle Ware
 */
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

require('./app.js')(app);

var server = app.listen(PORT, function(err){
    if (err) {
        console.log('Server Initialization error');
    }
    console.log('Express Server listening on port ', PORT);
});

module.exports = server;
# Seattle Pet Tracker #
___
Repository for Seattle Pet Tracking App
Managing Software Development
Fall - 2017

### Team Members ###
___
* Brittany Haffner
* David Agumya
* Shravanthi Rajagopal

### Application ###
___

[Seattle Pet Tracker](https://seattle-pet-tracker.herokuapp.com)

### How to use the application ###
___

[Swagger Documentation](https://app.swaggerhub.com/apis/shravass/seattle-pet_tracker/1.0.2)


# Note 
___
+ Original project repo with commit history and issues is in a private repository. This is a new repo to give public access
+ **For further information refer to the project WIKI**

# Personal Contribution 
___
+ Adaption of high level problem to potential features needed and data model relations.
+ Contributed to design of data model (refer to Wiki )
+ Implementation of User, Pet Branch and Location route API layer , service layer and ORM Layer
+ Feature Testing using Chai HTTP for all corresponsing routes and features implemented.


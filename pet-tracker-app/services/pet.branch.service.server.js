/**
 * Created by shravass on 6/22/17.
 */

    //initializing q
var q = require('q');


// json schema validation instance
var Validator = require('jsonschema').Validator;
var petBranchValidator = new Validator();

// Pet Branch schema defined for validation
var petBranchSchema = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Pet Branch",
    "description": "Branch details",
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "branchUniqueId": {"type": "string"},
        "inventory": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "category": {"type": "string", "enum": ["SMALL", "MEDIUM", "LARGE"]},
                    "capacity": {"type": "number"}
                },
                "additionalProperties": false,
                "required": ["category", "capacity"]
            },
            "minItems": 3,
            "uniqueItems": true
        },
        "noOfStaff": {"type": "integer"},
        "streetAddress": {
            "description": "Street address for a branch",
            "type": "string"
        },
        "city": {
            "description": "City of the branch",
            "type": "string"
        },
        "state": {
            "description": "State of the branch",
            "type": "string"
        },
        "zipcode": {
            "description": "Zip code of the branch",
            "type": "integer"
        },
        "contact": {
            "description": "contact number of the branch",
            "type": "string"
        }
    },
    "additionalProperties": false,
    "required": ["name", "branchUniqueId", "inventory", "noOfStaff", "streetAddress", "city", "state", "zipcode", "contact"]
};

var valid_categories = ['SMALL', 'MEDIUM', 'LARGE'];


// getting model
var petBranchModel = require('../model/pet_branch/pet_branch.model.server');

// defining service methods
module.exports.createPetBranch = createPetBranch;
module.exports.findPetBranchByAvailability = findPetBranchByAvailability;
module.exports.findPetBranchByBranchUniqueId = findPetBranchByBranchUniqueId;
module.exports.findPetBranch = findPetBranch;
module.exports.updatePetBranch = updatePetBranch;
module.exports.deletePetBranch = deletePetBranch;
module.exports.findAllPetBranches = findAllPetBranches;
module.exports.updateInventoryDetails = updateInventoryDetails;
module.exports.checkBranchInventory = checkBranchInventory;
module.exports.reactivatePetBranch = reactivatePetBranch;
module.exports.hardDelete = hardDelete;
module.exports.hardCreate = hardCreate;


/** Method that creates given pet branch in the system.
 * Validations:
 * Checks the input against json schema
 * Checks if branch details are already present in the system
 * Returns:
 * The created pet branch json hiding mongo db details
 **/
function createPetBranch(pet_branch) {
    var defer = q.defer();
    var schemaValidation = petBranchValidator.validate(pet_branch, petBranchSchema);
    if (schemaValidation.valid) {
        petBranchModel.findPetBranchByBranchUniqueId(pet_branch.branchUniqueId)
            .then(function (petBranch) {
                if (petBranch) {
                    defer.reject(' PetBranch details already exist ');
                }
                else {
                    //inventory uniqueness check
                    var inventoryArray = [];
                    for (var i = 0; i < pet_branch.inventory.length; i++) {
                        if(inventoryArray.indexOf(pet_branch.inventory[i].category) !== -1){
                            defer.reject(" Duplicate inventory for same category " + pet_branch.inventory[i].category);
                            return defer.promise;
                        }
                        inventoryArray.push(pet_branch.inventory[i].category);
                    }

                    for (var j = 0; j < pet_branch.inventory.length; j++) {
                        pet_branch.inventory[j].allocated = 0;
                        pet_branch.inventory[j].availability = pet_branch.inventory[j].capacity - pet_branch.inventory[j].allocated;
                    }
                    petBranchModel.createPetBranch(pet_branch).then(function (pet_branch) {
                        if (pet_branch) {
                            delete pet_branch._doc._id;
                            delete pet_branch._doc.__v;
                            for (var j = 0; j < pet_branch.inventory.length; j++) {
                                delete pet_branch.inventory[j]._doc._id;
                            }
                            defer.resolve(pet_branch);
                        } else {
                            defer.reject(' PetBranch could not be created ');
                        }
                    }, function (err) {
                        defer.reject(' PetBranch could not be created: ' + err);
                    })
                }
            }, function (err) {
                defer.reject(' PetBranch could not be created: ' + err);
            })

    }
    else {
        defer.reject(' PetBranch could not be created: ' + schemaValidation.errors);
    }

    return defer.promise;
}

/** Method to find available array of branches for the given pet category.
 * Validations:
 * Checks if category value is correct
 * Returns:
 * The array of pet branches found with availability in the given pet category
 **/
function findPetBranchByAvailability(category_input) {
    var defer = q.defer();
    var category = category_input.toUpperCase();
    if (category && valid_categories.indexOf(category) > -1) {
        petBranchModel.findPetBranchByAvailability(category).then(function (branches) {
            if (branches) {
                defer.resolve(branches);
            }
            else {
                defer.reject(" No branches available for this category ");
            }

        }, function (err) {
            defer.reject(' PetBranch could not be found: ' + err);
        });
    }
    else {
        defer.reject(" Please specify a valid category ");
    }
    return defer.promise;
}

/** Method to find branch by branch unique id.
 * Returns:
 * The pet branch json hiding mongo db details for the given branch unique id
 **/
function findPetBranch(branchId) {
    var defer = q.defer();
    petBranchModel.findPetBranchByBranchUniqueId(branchId)
        .then(function (branch) {
            if (branch) {
                defer.resolve(branch);
            }
            else {
                defer.reject(" Branch details could not be found ");
            }
        }, function (err) {
            defer.reject(" Branch details could not be found " + err);
        });
    return defer.promise;
}

/** Method to find branch by branch unique id.
 * Returns:
 * The pet branch json with mongo db details for the given branch unique id
 **/
function findPetBranchByBranchUniqueId(branchId) {
    var defer = q.defer();
    petBranchModel.findPetBranchByBranchUniqueIdWithId(branchId)
        .then(function (branch) {
            if (branch) {
                if (branch.active) {
                    defer.resolve(branch);
                }
                else {
                    defer.reject("Branch no longer active")
                }
            }
            else {
                defer.reject(" Branch details could not be found ");
            }
        }, function (err) {
            defer.reject(" Branch details could not be found " + err);
        });
    return defer.promise;
}

/** Method that creates given pet branch in the system.
 * Validations:
 * Checks the input against json schema
 * Checks if branch details are present in the system for the branch unique id present in input
 * Returns:
 * The updated pet branch json hiding mongo db details
 **/
function updatePetBranch(pet_branch) {
    var defer = q.defer();
    var schemaValidation = petBranchValidator.validate(pet_branch, petBranchSchema);
    if (schemaValidation.valid) {
        findPetBranchByBranchUniqueId(pet_branch.branchUniqueId)
            .then(function (branch) {
                if (branch) {
                    pet_branch._id = branch._id;

                    //inventory uniqueness check
                    var inventoryArray = [];
                    for (var i = 0; i < pet_branch.inventory.length; i++) {
                        if(inventoryArray.indexOf(pet_branch.inventory[i].category) !== -1){
                            defer.reject(" Duplicate inventory for same category " + pet_branch.inventory[i].category);
                            return defer.promise;
                        }
                        inventoryArray.push(pet_branch.inventory[i].category);
                    }

                    for (var j = 0; j < pet_branch.inventory.length; j++) {
                        pet_branch.inventory[j].allocated = branch.inventory[j].allocated;
                        pet_branch.inventory[j].availability = pet_branch.inventory[j].capacity - pet_branch.inventory[j].allocated;
                    }
                    petBranchModel.updatePetBranch(pet_branch)
                        .then(function (branch) {
                            if (branch) {
                                delete branch._doc._id;
                                delete branch._doc.__v;
                                for (var j = 0; j < branch.inventory.length; j++) {
                                    delete branch.inventory[j]._doc._id;
                                }
                                defer.resolve(branch);
                            }
                            else {
                                defer.reject(' PetBranch details not found ');
                            }
                        }, function (err) {
                            defer.reject(' PetBranch could not be updated: ' + err);
                        })
                }
                else {
                    defer.reject(" Branch details could not be found ");
                }
            }, function (err) {
                defer.reject(" Branch details could not be found " + err);
            });
    }
    else {
        defer.reject(' PetBranch could not be updated: ' + schemaValidation.errors);
    }
    return defer.promise;
}

/**
 * Function to see if the branch is void of animals before deleting
 * @param branchId
 */
function checkIfEmpty(branchId) {
    var defer = q.defer();
    petBranchModel.checkAllocated(branchId, "SMALL").then(function (branch) {
        if (branch) {
            petBranchModel.checkAllocated(branchId, "MEDIUM").then(function (branch) {
                if (branch) {
                    petBranchModel.checkAllocated(branchId, "LARGE").then(function (branch) {
                        if (branch) {
                            defer.resolve(branch); //branch is good to be deleted
                        }
                        else {
                            defer.reject("Animals need to be relocated before deleting branch")
                        }
                    }, function (err) {
                        defer.reject(err)
                    });
                }
                else {
                    defer.reject("Animals need to be relocated before deleting branch")
                }
            }, function (err) {
                defer.reject(err)
            });
        }
        else {
            defer.reject("Animals need to be relocated before deleting branch");
        }
    }, function (err) {
        defer.reject(err)
    });

    return defer.promise;
}

/** Method to delete branch by branch unique id.
 * Returns:
 * The deleted pet branch json hiding mongo db details for the given branch unique id
 **/
function deletePetBranch(branchId) {
    var defer = q.defer();
    findPetBranchByBranchUniqueId(branchId).then(function (branch) {
            checkIfEmpty(branchId).then (function (branch) {
                petBranchModel.deletePetBranch(branchId).then(function (branch) {
                    if (branch) {
                        defer.resolve(branch);
                    }
                    else {
                        defer.reject(' PetBranch could not be deleted. Incorrect pet branch id');
                    }
                }, function (err) {
                    defer.reject(' PetBranch could not be deleted: ' + err);
                });
            }, function (err) {
                defer.reject(err);
            });
    }, function (err) {
        defer.reject(' PetBranch could not be deleted: ' + err);
    });

    return defer.promise;
}

/**
 * Function to reactivate pet branch that has been deleted
 * @param branchId
 */
function reactivatePetBranch(branchId) {
    var defer = q.defer();
    petBranchModel.reactivatePetBranch(branchId).then(function (branch) {
        if (branch) {
            defer.resolve(branch)
        }
        else {
            defer.reject("Could not reactivate branch. Check that correct ID was used")
        }
    }, function(err) {
        defer.reject("Branch could not be reactivated: " + err);
    });

    return defer.promise;
}

/** Method to find all pet branches available in the system.
 * Returns:
 * The array of pet branches hiding mongo db details
 **/
function findAllPetBranches() {
    var defer = q.defer();
    petBranchModel.findAllPetBranches().then(function (petBranches) {
        if (petBranches) {
            defer.resolve(petBranches);
        }
        else {
            defer.reject(' PetBranch details not found ');
        }
    }, function (err) {
        defer.reject(' PetBranch details not found ' + err);
    });
    return defer.promise;
}

// Pet Branch schema defined for validation
var inventoryUpdateSchema = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Pet Branch Inventory",
    "description": "Branch Inventory details",
    "type": "object",
    "properties": {
        "branchUniqueId": {"type": "string"},
        "category": {"type": "string", "enum": ["SMALL", "MEDIUM", "LARGE"]},
        "noOfPetsAllocated": {"type": "number"}
    },
    "additionalProperties": false,
    "required": ["branchUniqueId", "category", "noOfPetsAllocated"]
};

/** Method that updates inventory for a particular branch based on input details.
 * Validations:
 * Checks the input against json schema
 * Checks if branch details are present in the system for the branch unique id present in input
 * Checks if branch can accomodate the given number of pets under given category
 * Returns:
 * The updated pet branch details json hiding mongo db details
 **/
function updateInventoryDetails(inventoryDetails) {
    var defer = require('q').defer();
    var schemaValidation = petBranchValidator.validate(inventoryDetails, inventoryUpdateSchema);
    if (schemaValidation.valid) {
        findPetBranchByBranchUniqueId(inventoryDetails.branchUniqueId)
            .then(function (branch) {
                if (branch) {
                    for (var j = 0; j < branch.inventory.length; j++) {
                        if (branch.inventory[j].category === inventoryDetails.category) {
                            var capacityToBeUpdated = branch.inventory[j].allocated + inventoryDetails.noOfPetsAllocated;
                            if (capacityToBeUpdated > branch.inventory[j].capacity) {
                                defer.reject(' PetBranch cannot accomodate this many pets');
                                return defer.promise;
                            }
                            else if (capacityToBeUpdated < 0) {
                                //keep allocated value at no lower than
                                branch.inventory[j].allocated = 0;
                                branch.inventory[j].availability = branch.inventory[j].capacity - branch.inventory[j].allocated;
                            }
                            branch.inventory[j].allocated += inventoryDetails.noOfPetsAllocated;
                            branch.inventory[j].availability = branch.inventory[j].capacity - branch.inventory[j].allocated;
                        }
                    }
                    petBranchModel.updatePetBranch(branch)
                        .then(function (branch) {
                            if (branch) {
                                delete branch._doc._id;
                                delete branch._doc.__v;
                                for (var j = 0; j < branch.inventory.length; j++) {
                                    delete branch.inventory[j]._doc._id;
                                }
                                defer.resolve(branch);
                            }
                            else {
                                defer.reject(' PetBranch details not found ');
                            }
                        }, function (err) {
                            defer.reject(' PetBranch could not be updated: ' + err);
                        })
                }
                else {
                    defer.reject(" Branch details could not be found ");
                }
            }, function (err) {
                defer.reject(" Branch details could not be found " + err);
            });
    }
    else {
        defer.reject(' PetBranch could not be updated: ' + schemaValidation.errors);
    }
    return defer.promise;
}

/** Method to find branch by branch unique id.
 * Returns:
 * The pet branch json hiding mongo db details for the given branch unique id
 **/
function checkBranchInventory(branchId, category) {
    var defer = q.defer();
    petBranchModel.checkAvailability(branchId, category)
        .then(function (branch) {
            if (branch) {
                defer.resolve(branch);
            }
            else {
                defer.reject("Branch details could not be found");
            }

        }, function (err) {
            defer.reject(" Branch details could not be found " + err);
        });
    return defer.promise;
}

function hardDelete(branchId) {
    var defer = q.defer();
    petBranchModel.hardDeletePetBranch(branchId).then(function (branch) {
        if(branch){
            defer.resolve({deleted_branch : branch})
        } else {
            defer.reject('Branch could not be deleted');
        }
    }).catch(function (err) {
        defer.reject(err)
    });
    return defer.promise;
}

function hardCreate(branch) {
    var defer = q.defer();
    petBranchModel.createPetBranch(branch).then(function (branch) {
        if(branch){
            defer.resolve(branch)
        } else{
            defer.reject('Branch could not be created');
        }
    }).catch(function (err) {
        defer.reject(err)
    });
    return defer.promise;
}

/**
 * Created by david on 10/11/17.
 */
/**
 *
 */

var locationSchema = require('./location.schema.server');
var mongoose = require('mongoose');

var locationModel = mongoose.model('LocationModel', locationSchema);

locationModel.createPetLocation = createPetLocation;
locationModel.findCurrentLocationOfPet = findCurrentLocationOfPet;
locationModel.findPetLocations = findPetLocations;
locationModel.findCurrentPetsInBranch = findCurrentPetsInBranch;
locationModel.findPetsHistoryInBranch = findPetsHistoryInBranch;
locationModel.updateLocEndDate = updateLocEndDate;
locationModel.deleteLocationsByPet = deleteLocationsByPet;
locationModel.deleteLocation = deleteLocation;
locationModel.updateLocEndDateAndOwner = updateLocEndDateAndOwner;

module.exports = locationModel;

/**
 * Given a location object with the location details for a pet, create pet location and save it to database
 * @param location -. Location Object
 * @returns {location}
 */
function createPetLocation(location) {
    "use strict";
    return locationModel.create(location)

}

/**
 *  Find a current location of pet specified by the given microchip id
 *  @param pet pet object
 *  @returns {Query}
 */
function findCurrentLocationOfPet(pet) {
    "use strict";
    return locationModel.findOne({Pet: pet, endDate: null})
        .populate({path: 'Branch', select: '-_id -__v -inventory'})
        .populate({path: 'Owner', select: '-_id firstName lastName phone'})
        .select({
            _id: 0,
            __v: 0,
            Pet: 0
        });
}
/**
 *  Given a petId, return all the location records on the pets
 * @param pet -> pet object, whose locations we retrieve
 * @returns {Query|*}
 */
function findPetLocations(pet) {
    "use strict";
    return locationModel.find({Pet: pet})
        .sort({startDate: -1})
        .populate({path: 'Branch', select: '-_id -__v -inventory'})
        .populate({path: 'Owner', select: '-_id firstName lastName phone'})
        .select({
            _id: 0,
            __v: 0,
            Pet: 0
        });
}
/**
 *  Return the information about current pets in the branch specified by branch Id
 * @param branch -> branch object
 * @returns {Query|*}
 */
function findCurrentPetsInBranch(branch) {
    "use strict";
    return locationModel.find({
        Branch: branch, endDate: null
    })
        .populate({path: 'Pet', select: '-_id -__v'})
        .populate({path: 'Owner', select: '-_id firstName lastName phone'})
        .select({
            _id: 0,
            __v: 0,
            Branch: 0
        });
}
/**
 *  Given a branch id, return all the location records on the branch
 * @param branch -> The branch , whose locations we retrieve
 * @returns {Query|*}
 */
function findPetsHistoryInBranch(branch) {
    "use strict";
    return locationModel.find({Branch: branch})
        .sort({startDate: -1})
        .populate({path: 'Pet', select: '-_id -__v'})
        .populate({path: 'Owner', select: '-_id firstName lastName phone'})
        .select({
            _id: 0,
            __v: 0,
            Branch: 0
        });
}
/**
 * Update the location record of a pet with the end date passed into the function
 * @param pet
 * @param end_date -> end date for the location. The last day the pet was at that specified location
 * @returns {Query|*}
 */
function updateLocEndDate(pet, end_date) {
    "use strict";
    return locationModel.findOneAndUpdate({Pet: pet, endDate: null}, {
        $set: {endDate: end_date}
    }, {new : true})
        .populate({path: 'Branch', select: '-_id -__v -inventory'})
        .populate({path: 'Owner', select: '-_id firstName lastName phone'})
        .select({
            _id: 0,
            __v: 0,
            Pet: 0
        });
}
/**
 * Given a location id, delete the location specified by that id.
 * @param locationId -> Location id for the location we are deleting
 * @returns {Query|*}
 */
function deleteLocation(locationId) {
    "use strict";
    return locationModel.findOneAndRemove({_id: locationId})
}

/**
 * Given a pet id, delete all the locations specified by that pet id.
 * @param locationId -> Location id for the location we are deleting
 * @returns {Query|*}
 */
function deleteLocationsByPet(petId) {
    "use strict";
    return locationModel.remove({Pet: petId})
        .sort({startDate: -1})
        .populate({path: 'Branch', select: '-_id -__v -inventory'})
        .populate({path: 'Owner', select: '-_id firstName lastName phone'})
        .select({
            _id: 0,
            __v: 0,
            Pet: 0
        });
}

function updateLocEndDateAndOwner(pet, end_date, owner) {
    "use strict";
    return locationModel.findOneAndUpdate({Pet: pet, endDate: null}, {
        $set: {endDate: end_date, Owner: owner}
    }, {new : true})
        .populate({path: 'Branch', select: '-_id -__v -inventory'})
        .populate({path: 'Owner', select: '-_id firstName lastName phone'})
        .select({
            _id: 0,
            __v: 0,
            Pet: 0
        });
}
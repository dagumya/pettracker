/**
 * Created by shravass on 6/22/17.
 */

    //initializing q
var q = require('q');

// json schema validation instance
var Validator = require('jsonschema').Validator;
var foundStatusValidator = new Validator();

// Pet Branch schema defined for validation
var foundStatusSchema = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Pet Found status",
    "description": "Found details",
    "type": "object",
    "properties": {
        "petId": {"type": "string"},
        "latitude": {"type": "number"},
        "longitude": {"type": "number"},
        "foundDate": {
            "description": "date on which pet was found",
            "type": "string"
        },
        "foundBy": {
            "description": "user name of person who found the pet",
            "type": "string"
        }
    },
    "additionalProperties": false,
    "required": ["petId", "latitude", "longitude", "foundBy"]
};


// getting model
var foundStatusModel = require('../model/found_status/found_status.model.server');

//getting other services
var petService = require('./pet.service.server');
var userService = require('./user.service.server');

// defining service methods
module.exports.createFoundStatus = createFoundStatus;
module.exports.getNearbyFoundPets = getNearbyFoundPets;
module.exports.getPetFoundStatusByMicrochipId = getPetFoundStatusByMicrochipId;
module.exports.updateFoundStatus = updateFoundStatus;
module.exports.deleteFoundStatus = deleteFoundStatus;

function createFoundStatus(found_status) {
    var defer = q.defer();
    var schemaValidation = foundStatusValidator.validate(found_status, foundStatusSchema);
    if (schemaValidation.valid) {
        petService.findPetByMicrochipID(found_status.petId)
            .then(function (pet) {
                found_status.Pet = pet;
                return userService.findUserByUserName(found_status.foundBy)
            })
            .then(function (user) {
                if (user) {
                    found_status.FoundBy = user;
                }
                return createFoundStatusRecord(found_status)
            })
            .then(function (found_status) {
                defer.resolve(found_status);
            })
            .catch(function (err) {
                defer.reject(err);
            })
    }
    else {
        defer.reject(' Found Status could not be created: ' + schemaValidation.errors);
    }
    return defer.promise;
}

function createFoundStatusRecord(found_status) {
    var defer = q.defer();
    foundStatusModel.findFoundStatusByPetId(found_status.Pet)
        .then(function (foundStatus) {
            if (foundStatus) {
                defer.reject(' Found Status already exists for the pet ');
            }
            else {
                if (found_status.foundDate) {
                    var date = new Date(found_status.foundDate);
                    if (date === 'Invalid Date' || date === 'NaN') {
                        defer.reject(' Invalid Date Format. Found Status could not be created: ' + found_status.foundDate);
                        return defer.promise;
                    }
                    found_status.foundDate = date;
                }

                found_status.location = {
                    type: "Point",
                    coordinates: [found_status.longitude, found_status.latitude]
                };
                delete found_status.latitude;
                delete found_status.longitude;
                delete found_status.petId;
                delete found_status.foundBy;

                foundStatusModel.createFoundStatus(found_status).then(function (found_status) {
                    if (found_status) {
                        defer.resolve(found_status);
                    } else {
                        defer.reject(' Found Status could not be created ');
                    }
                }, function (err) {
                    defer.reject(' Found Status could not be created: ' + err);
                })
            }
        }, function (err) {
            defer.reject(' Found Status could not be created: ' + err);
        });
    return defer.promise;
}

// Co ordinates schema defined for validation

var coordinatesSchema = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Location status",
    "description": "Location details",
    "type": "object",
    "properties": {
        "latitude": {"type": "number"},
        "longitude": {"type": "number"},
        "distance": {"type": "number"}
    },
    "additionalProperties": false,
    "required": ["latitude", "longitude"]
};

function getNearbyFoundPets(coordinates) {
    var defer = q.defer();
    var schemaValidation = foundStatusValidator.validate(coordinates, coordinatesSchema);
    if (schemaValidation.valid) {
        foundStatusModel.findNearByPets(coordinates).then(function (foundStatusList) {
            defer.resolve(foundStatusList);
        }, function (err) {
            defer.reject(" Found Status could not be found: " + err);
        });
    } else {
        defer.reject(' Found Status could not be found: ' + schemaValidation.errors);
    }
    return defer.promise;
}

function getPetFoundStatusByMicrochipId(microchipId) {
    var defer = q.defer();
    petService.findPetByMicrochipID(microchipId)
        .then(function (pet) {
            return foundStatusModel.findFoundStatusByPetId(pet);
        })
        .then(function (found_status) {
            if (found_status) {
                defer.resolve(found_status);
            }
            else {
                defer.reject("Pet doesn't have a found status record");
            }
        })
        .catch(function (err) {
            defer.reject(" Found Status could not be found: " + err);
        });

    return defer.promise;

}

var foundStatusUpdateSchema = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Pet Found status",
    "description": "Found details",
    "type": "object",
    "properties": {
        "petId": {"type": "string"},
        "latitude": {"type": "number"},
        "longitude": {"type": "number"},
        "foundDate": {
            "description": "date on which pet was found",
            "type": "string"
        },
        "foundBy": {
            "description": "user name of person who found the pet",
            "type": "string"
        }
    },
    "additionalProperties": false,
    "required": ["petId"],
    "dependencies": {
        "latitude": ["longitude"],
        "longitude": ["latitude"]
    }
};

function updateFoundStatus(foundStatusToBeUpdated) {
    var defer = q.defer();
    var schemaValidation = foundStatusValidator.validate(foundStatusToBeUpdated, foundStatusUpdateSchema);
    if (schemaValidation.valid) {
        petService.findPetByMicrochipID(foundStatusToBeUpdated.petId)
            .then(function (pet) {
                foundStatusToBeUpdated.Pet = pet;
                if (foundStatusToBeUpdated.foundBy) {
                    return userService.findUserByUserName(foundStatusToBeUpdated.foundBy)
                }
                else {
                    return Promise.resolve();
                }
            })
            .then(function (user) {
                if (user) {
                    foundStatusToBeUpdated.FoundBy = user;
                }
                return updateFoundStatusRecord(foundStatusToBeUpdated)
            })
            .then(function (found_status) {
                defer.resolve(found_status);
            })
            .catch(function (err) {
                defer.reject(err);
            })
    }
    else {
        defer.reject(' Found Status could not be updated: ' + schemaValidation.errors);
    }
    return defer.promise;
}

function updateFoundStatusRecord(foundStatusTobeUpdated) {
    var defer = q.defer();
    foundStatusModel.findFoundStatusByPetIdWithId(foundStatusTobeUpdated.Pet)
        .then(function (existingFoundStatus) {
            if (existingFoundStatus) {

                //set id
                foundStatusTobeUpdated._id = existingFoundStatus._id;

                //set found date
                if (foundStatusTobeUpdated.foundDate) {
                    var date = new Date(foundStatusTobeUpdated.foundDate);
                    if (date === 'Invalid Date' || date === 'NaN') {
                        defer.reject(' Invalid Date Format. Found Status could not be created: ' + foundStatusTobeUpdated.foundDate);
                        return defer.promise;
                    }
                    foundStatusTobeUpdated.foundDate = date;
                } else {
                    foundStatusTobeUpdated.foundDate = existingFoundStatus.foundDate;
                }

                //set location
                if (foundStatusTobeUpdated.longitude && foundStatusTobeUpdated.latitude) {
                    foundStatusTobeUpdated.location = {
                        type: "Point",
                        coordinates: [foundStatusTobeUpdated.longitude, foundStatusTobeUpdated.latitude]
                    };
                    delete foundStatusTobeUpdated.latitude;
                    delete foundStatusTobeUpdated.longitude;
                }
                else {
                    foundStatusTobeUpdated.location = existingFoundStatus.location;
                }

                // set found by default
                if (!foundStatusTobeUpdated.FoundBy) {
                    foundStatusTobeUpdated.FoundBy = existingFoundStatus.FoundBy;
                }

                delete foundStatusTobeUpdated.petId;
                delete foundStatusTobeUpdated.foundBy;


                foundStatusModel.updateFoundStatus(foundStatusTobeUpdated).then(function (found_status) {
                    if (found_status) {
                        defer.resolve(found_status);
                    }
                    else {
                        defer.reject(' Found Status could not be updated ');
                    }
                }, function (err) {
                    defer.reject(' Found Status could not be updated: ' + err);
                })
            }
            else {
                defer.reject(' Could not get found status for the given pet ');
            }
        }, function (err) {
            defer.reject(' Found Status could not be updated: ' + err);
        });
    return defer.promise;
}

function deleteFoundStatus(microchipId) {
    var defer = q.defer();
    petService.findPetByMicrochipID(microchipId)
        .then(function (pet) {
            return foundStatusModel.deleteFoundStatusByPet(pet);
        })
        .then(function (found_status) {
            if (found_status) {
                defer.resolve(found_status);
            }
            else {
                defer.reject(" Found Status could not be deleted ");
            }
        })
        .catch(function (err) {
            defer.reject(" Found Status could not be deleted: " + err);
        });

    return defer.promise;
}

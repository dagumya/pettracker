/**
 * Created by shravass on 7/6/17.
 */
var foundStatusSchema = require('./found_status.schema.server');

var mongoose = require('mongoose');

var foundStatusModel = mongoose.model('FoundStatusModel', foundStatusSchema);

foundStatusModel.createFoundStatus = createFoundStatus;
foundStatusModel.findFoundStatusById = findFoundStatusById;
foundStatusModel.findFoundStatusByPetId = findFoundStatusByPetId;
foundStatusModel.findNearByPets = findNearByPets;
foundStatusModel.updateFoundStatus = updateFoundStatus;
foundStatusModel.deleteFoundStatusByPet = deleteFoundStatusByPet;
foundStatusModel.findFoundStatusByPetIdWithId = findFoundStatusByPetIdWithId;

module.exports = foundStatusModel;


function createFoundStatus(foundStatus) {
    return foundStatusModel.create(foundStatus);
}

function findFoundStatusByPetId(petId) {
    return foundStatusModel.findOne({Pet: petId})
        .populate({path: 'Pet', select: '-_id microchipId'})
        .populate({path: 'FoundBy', select: '-_id firstName lastName phone'})
        .select({
            _id: 0,
            __v: 0,
            "location._id": 0
        });
}

function findFoundStatusByPetIdWithId(petId) {
    return foundStatusModel.findOne({Pet: petId});
}

function findFoundStatusById(foundStatusId) {
    return foundStatusModel.findOne({_id: foundStatusId});
}

function findNearByPets(coordinates) {
    var distance = 8000;
    if (coordinates.distance) {
        distance = coordinates.distance;
    }
    return foundStatusModel.find({
        location: {
            $near: {
                $geometry: {type: "Point", coordinates: [coordinates.longitude, coordinates.latitude]},
                $maxDistance: distance
            }
        }
    })
        .populate({path: 'Pet', select: '-_id -__v'})
        .populate({path: 'FoundBy', select: '-_id firstName lastName phone'})
        .select({
            _id: 0,
            __v: 0,
            "location._id": 0
        });
}

function updateFoundStatus(updateFoundStatus) {
    return foundStatusModel.findOneAndUpdate({_id: updateFoundStatus._id}, {$set: updateFoundStatus}, {new: true});
}

function deleteFoundStatusByPet(petId) {
    return foundStatusModel.findOneAndRemove({Pet: petId})
        .populate({path: 'Pet', select: '-_id microchipId'})
        .populate({path: 'FoundBy', select: '-_id firstName lastName phone'})
        .select({
            _id: 0,
            __v: 0,
            "location._id": 0
        });
}
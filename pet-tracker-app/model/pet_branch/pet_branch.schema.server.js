/**
 * Created by shravass on 7/6/17.
 */

var mongoose = require('mongoose');

var inventorySchema = mongoose.Schema({

    category: {type: String, required: true, enum: ['SMALL', 'MEDIUM', 'LARGE']},
    capacity: {type: Number, required: true},
    allocated: {type: Number, required: true},
    availability: {type: Number, required: true}


});

var branchSchema = mongoose.Schema({

    name: {type: String, required: true},
    branchUniqueId: {type: String, unique: true, required: true},
    inventory: {type: [{type: inventorySchema}], required: true},
    noOfStaff: {type: Number, required: true},
    streetAddress: {type: String, required: true},
    city: {type: String, required: true},
    state: {type: String, required: true},
    zipcode: {type: Number, required: true},
    contact: {type: String, required: true},
    active: {type: Boolean, default: true}


}, {collection: 'Branch'});

module.exports = branchSchema;

/**
 * Created by david on 10/24/17.
 */
process.env.MONGO_LOCAL = "localhost/animal_tracker";
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');
var should = chai.should();
chai.use(chaiHttp);

/**
 * IMPORT MODELS TO SAVE TEST DATA BEFORE RUNNING TESTS
 */
var userService = require("../services/user.service.server");
var apiKey = "";

var sample_admin = {
    username: "admin",
    password: "pas345yet",
    apiKey: "dfhghjhhut123",
    firstName: "admin",
    lastName: "admin",
    email: "geod@gmail.com",
    phone: "8964563452",
    streetAddress: "123 Common Street",
    city: "Seattle",
    state: "Washington",
    zipcode: 34521,
    role: ['ADMIN','USER'],
    active: true

};

var adminApiKey = "dfhghjhhut123";

describe("Test User Routes", function() {

    before(function (done) {
        userService.hardCreate(sample_admin)
                .then(function (user) {
                    if (user) {
                        console.log("Admin was created");
                        done();
                    } else {
                        console.log("Admin was not deleted");
                        done();
                    }
                }).catch(function (err) {
                console.log(err);
                done();
            });
        }
    );

    // now test the create route
    describe("POST /user", function(){
        "use strict";
        it("it should create a new user", function(done){
            var test_user = {
                firstName    : "David",
                lastName     : "Geoffrey",
                streetAddress: "123 34th Ave North",
                city         : "Seattle",
                state		 : "WA",
                zipcode      : 98563,
                phone        : "6735463456",
                username     : "dgeoffrey",
                password     : "1234password",
                email        : "dgeoff45@gmail.com"
            };
            chai.request(server)
                .post('/user')
                .send(test_user)
                .end(function(err, res){
                    if (err){ return console.log("error" + err.message)}
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('firstName').eql('David');
                    res.body.should.have.property('lastName').eql('Geoffrey');
                    res.body.should.have.property('streetAddress').eql('123 34th Ave North');
                    res.body.should.have.property('city').eql('Seattle');
                    res.body.should.have.property('state').eql('WA');
                    res.body.should.have.property('zipcode').eql(98563);
                    res.body.should.have.property('username').eql('dgeoffrey');
                    res.body.should.have.property('email').eql('dgeoff45@gmail.com');
                    res.body.should.have.property('phone').eql('6735463456');
                    res.body.should.have.property('role');
                    res.body.role.should.be.a('array');
                    res.body.role.should.include('USER');

                    done()
                })
        })
    });

    describe("GET /user?username=*&password=*", function(){
        "use strict";
        it("it should get apiKey for correct username and password combination ", function(done){
            chai.request(server)
                .get("/user?username=dgeoffrey&password=1234password")
                .send()
                .end(function (err, res) {
                    res.should.have.status(200);
                    //save api key
                    apiKey = res.body.api_key;
                    done()
                })
        })
    });

    describe("POST /user", function(){
        "use strict";
        it("it should fail to create a new user because username has to be unique", function(done){
            var test_user2 = {
                firstName    : "Daniel",
                lastName     : "Mudridge",
                streetAddress: "123 34th Ave North",
                city         : "New York ",
                state		 : "New York",
                zipcode      : 87245,
                phone        : "9872346598",
                username     : "dgeoffrey",
                password     : "diffpassword",
                email        : "daniel_m@gmail.com"
            };
            chai.request(server)
                .post('/user')
                .send(test_user2)
                .end(function(err, res){
                    res.should.have.status(400);
                    done()
                })
        })
    });

    describe("POST /user", function(){
        "use strict";
        it("it should fail to create a new user because the request body is empty", function(done){
            var test_user3 = {};
            chai.request(server)
                .post('/user')
                .send(test_user3)
                .end(function(err, res){
                    res.should.have.status(400);
                    done()
                })
        })
    });

    describe("GET /user/:username", function(){
        "use strict";
        it("it should get user specified by the username ", function(done){
            chai.request(server)
                .get("/user/dgeoffrey?apiKey=" + apiKey)
                .send()
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('firstName').eql('David');
                    res.body.should.have.property('lastName').eql('Geoffrey');
                    res.body.should.have.property('streetAddress').eql('123 34th Ave North');
                    res.body.should.have.property('city').eql('Seattle');
                    res.body.should.have.property('state').eql('WA');
                    res.body.should.have.property('zipcode').eql(98563);
                    res.body.should.have.property('username').eql('dgeoffrey');
                    res.body.should.have.property('email').eql('dgeoff45@gmail.com');
                    res.body.should.have.property('phone').eql('6735463456');
                    res.body.should.have.property('role');
                    res.body.role.should.be.a('array');
                    res.body.role.should.include('USER');
                    done()
                })
        })
    });

    describe("GET /user?username=*&password=*", function(){
        "use strict";
        it("it should fail because of incorrect password ", function(done){
            chai.request(server)
                .get("/user?username=dgeoffrey&password=14password")
                .send()
                .end(function (err, res) {
                    res.should.have.status(400);
                    done()
                })
        })
    });

    describe("GET /user?username=*&password=*", function(){
        "use strict";
        it("it should fail because of incorrect username ", function(done){
            chai.request(server)
                .get("/user?username=dgeoff&password=1234password")
                .send()
                .end(function (err, res) {
                    res.should.have.status(400);
                    done()
                })
        })
    });

    describe("PUT /updateUserRole?apiKey=*&username=*&role=*", function(){
        "use strict";
        it("updates user by username assigns role of EMPLOYEE ", function(done){
            chai.request(server)
                .put("/updateUserRole?apiKey=" + adminApiKey +"&username=dgeoffrey&role=EMPLOYEE&operation=A")
                .send()
                .end(function (err, res) {
                    res.should.have.status(200);
                    done()
                })
        })
    });

    describe("PUT /updateUserRole?apiKey=*&username=*&role=*", function(){
        "use strict";
        it("updates user by username revokes role of EMPLOYEE ", function(done){
            chai.request(server)
                .put("/updateUserRole?apiKey=" + adminApiKey +"&username=dgeoffrey&role=EMPLOYEE&operation=R")
                .send()
                .end(function (err, res) {
                    res.should.have.status(200);
                    done()
                })
        })
    });

    describe("PUT /updateUserRole?apiKey=*&username=*&role=*", function(){
        "use strict";
        it("fails to update user as mandatory params are missing ", function(done){
            chai.request(server)
                .put("/updateUserRole?apiKey=" + adminApiKey +"&username=dgeoff")
                .send()
                .end(function (err, res) {
                    res.should.have.status(400);
                    done()
                })
        })
    });

    describe("PUT /updateUserRole?apiKey=*&username=*&role=*", function(){
        "use strict";
        it("fails to update user for invalid username ", function(done){
            chai.request(server)
                .put("/updateUserRole?apiKey=" + adminApiKey +"&username=mojo&role=EMPLOYEE&operation=R")
                .send()
                .end(function (err, res) {
                    res.should.have.status(400);
                    done()
                })
        })
    });

    describe("PUT /user/:username", function(){
        it("it should update a user's last name and address", function(done){
            var updates = {
                lastName : "Agumya",
                streetAddress: "123 56th Ave",
                city     : "Atlanta",
                state    : "GA",
                zipcode  : 56732
            };
            chai.request(server)
                .put('/user/dgeoffrey?apiKey=' + apiKey)
                .send(updates)
                .end(function(err, res){
                    if (err){ console.log("error")}
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('firstName').eql('David');
                    res.body.should.have.property('lastName').eql('Agumya');
                    res.body.should.have.property('streetAddress').eql('123 56th Ave');
                    res.body.should.have.property('city').eql('Atlanta');
                    res.body.should.have.property('state').eql('GA');
                    res.body.should.have.property('zipcode').eql(56732);
                    res.body.should.have.property('username').eql('dgeoffrey');
                    res.body.should.have.property('email').eql('dgeoff45@gmail.com');
                    res.body.should.have.property('phone').eql('6735463456');
                    res.body.should.have.property('role');
                    res.body.role.should.be.a('array');
                    res.body.role.should.include('USER');
                    done()

                })

        })

    });

    describe("DELETE /user/:username", function() {
        "use strict";
        it("it should deactivate a user record specified by the username", function(done){
            chai.request(server)
                .delete('/user/dgeoffrey?apiKey=' + apiKey)
                .send()
                .end(function(err, res){
                    res.should.have.status(200);
                    done()
                })
        })
    });

    describe("DELETE /user/:username", function() {
        "use strict";
        it("it should fail to deactivate a user since the user is already deactivate", function(done){
            chai.request(server)
                .delete('/user/dgeoffrey?apiKey=' + adminApiKey)
                .send()
                .end(function(err, res){
                    res.should.have.status(400);
                    done()
                })
        })
    });

    describe("PUT /user/:username", function(){
        it("it should fail to update because the user does not exist ", function(done){
            var updates = {
                lastName : "Mumbs",
                streetAddress: "123 56th Ave",
                city     : "Atlanta",
                state    : "GA",
                zipcode  : 56732
            };
            chai.request(server)
                .put('/user/dgeoffrey?apiKey=' + adminApiKey)
                .send(updates)
                .end(function(err, res){
                    res.should.have.status(400);
                    done()
                })
        })

    });

    // get /user/:username
    describe("GET /user/:username", function(){
        "use strict";
        it("it should fail to get user specified by the username because user is not in the system ", function(done){
            chai.request(server)
                .get("/user/dgeoffrey?apiKey=" + adminApiKey)
                .send()
                .end(function (err, res) {
                    res.should.have.status(400);
                    done()
                })
        })
    });

    describe("PUT /user/activate/:username", function() {
        "use strict";
        it("it should activate a user record specified by the username", function(done){
            chai.request(server)
                .put('/user/activate/dgeoffrey?apiKey=' + adminApiKey)
                .send()
                .end(function(err, res){
                    res.should.have.status(200);
                    done()
                })
        })
    });

    describe("PUT /user/activate/:username", function() {
        "use strict";
        it("it should fail to activate a user since the user is not in system", function(done){
            chai.request(server)
                .put('/user/activate/mojo?apiKey=' + adminApiKey)
                .send()
                .end(function(err, res){
                    res.should.have.status(400);
                    done()
                })
        })
    });


    after(function (done) {
            userService.hardDelete("dgeoffrey")
                .then(function (user) {
                if (user) {
                    console.log("User dgeoffrey was deleted");
                    return userService.hardDelete("admin");
                } else {
                    console.log("User dgeoffrey was not deleted");
                    done();
                }
            }).then(function (user) {
                if (user) {
                    console.log("Admin was deleted");
                    done();
                } else {
                    console.log("Admin was not deleted");
                    done();
                }
            }).catch(function (err) {
                console.log(err);
                done();
            });
        }
    );
});